﻿(function () {
    var common = require('../library/common.js'),
        fish = require('./fish.js'),
        fishtypes = require('../config/fish.json');

    /**
    *鱼管理类
    */    
    function fishmgr(server) {
        this.server = server;
        //鱼种类列表
        this.fishtypelist = {};
        //鱼种类数量
        this.typecount = 0;
        //每种鱼发的次数
        this.fishtypecount = {};
   }
   /**
   *  初始化
   */
   //fishmgr.prototype.init = function (callback) {
   //     var me = this;
   //     me.server.db.select("game_haiwang2_fish_type",'*', function (err, result) {        
   //         if (err) {
   //             me.server.debug('init fish type error:' + err);
   //             return common.invokecallback(callback, err, false);
   //         }
            
   //         for (var i = 0; i < result.length; i++) {                
   //             me.fishtypelist[result[i].type_id] = new fish(result[i]);
   //         }
   //         me.typecount = result.length;
   //         return common.invokecallback(callback, err,true);
   //     });
   // }  
    
    fishmgr.prototype.newfish = function (fishtype){
        return new fish(fishtypes[fishtype]);
    }

    module.exports = function (server) {
        return new fishmgr(server);
    }

})();
