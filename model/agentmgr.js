// JavaScript source code
(function () {
    var common = require('../library/common.js');

    function agentmgr(client) {
        this.client = client;
        this.agency_id;
        this.agency_status;
        this.agency_username;
        this.agency_follow_top_agentpool;
        this.agency_allow_create_pc;
        this.agency_allow_create_mobile;
        this.agency_upline_structure;
    };

    agentmgr.prototype.get = function (agencyid,callback) {
        var me = this;
        me.client.db.select('agency','*',[{agency_id:agencyid}],null,1, function (err, data) {
            if (err) { 
               return common.invokecallback(callback, err, null);
            }
            
            if (common.empty(data)) { 
                return common.invokecallback(callback, new Error('agencyid not exsit'), null); 
            }
            var res = data[0];
            me.agency_id = res.agency_id;
            me.agency_status = res.agency_status;
            me.agency_username = res.agency_username;
            me.agency_follow_top_agentpool = res.agency_follow_top_agentpool;
            me.agency_allow_create_pc = res.agency_allow_create_pc;
            me.agency_allow_create_mobile = res.agency_allow_create_mobile;
            me.agency_upline_structure = res.agency_upline_structure;
            return common.invokecallback(callback, null, me); 
        });
    };

    agentmgr.prototype.getstatus = function (suspend_agencyid, callback){
        var me = this;
        console.log(suspend_agencyid.split(',').length);
        me.client.db.select('agency_suspend','*', {agency_suspend_id: { "IN": suspend_agencyid.split(',')}}, function (err, data) {
            if (err) {
                return common.invokecallback(callback, err, null);
            }
            
            if (common.empty(data)) {
                return common.invokecallback(callback, new Error('agencyid not exsit'), null);
            }
            var status = true;
            for (var i = 0; i < data.length; i++) {
                if (data[i].agency_suspend_status != 1) {
                    status = false;
                    continue;
                }
            }
            return common.invokecallback(callback, null, status);
        });
    }

    module.exports = function (client) {
        return new agentmgr(client);
    };
})();