(function () {
    var common = require('../library/common.js');
    
    /**
     *游戏设定管理 
     **/
    function gamesetting(server) {
        this.server = server;
        this.name;
        this.type;
        this.data;
        this.status;
    }
    /**
     *初始化游戏设定  目前只做了从mysql取
     **/
    gamesetting.prototype.init = function (callback){
        var me = this;
        me.server.db.select("game_setting","*", function (err, data) {
            if (err) { 
                return common.invokecallback(callback, err, null);
            }
            var res= data[0];
            me.name = res.game_setting_name;
            me.type = res.game_setting_type;
            me.data = res.game_setting_data;
            me.status = res.game_setting_status;
            return common.invokecallback(callback, err, me);
        });
    }

    
    module.exports = function () {
        return new gamesetting();
    }

})();
