﻿(function () {
    var common = require('../library/common.js');

    kioskmgr = function (client) {
        this.client = client;
        //用户session
        this.kiosk_session;
        //用户id
        this.kiosk_id;
        //用户合约状态
        this.kiosk_status;
        //用户代理id
        this.kiosk_agencyid;
        //用户名称
        this.kiosk_username;
        //用户钱包a
        this.kiosk_balance_a;
        this.kiosk_balance_b;
        this.kiosk_balance_c;
        
        //用户座位
        this.seat_id = -1;
        //用户当前下注金额
        this.betstake = 0;
        
        //用户当局下注累计
        this.bettotal = 0;
        
        //当局中奖金额累计
        this.wintotal = 0;
        
        //用户当前使枪支类型
        this.guntype = 0;
        
        //用户的子弹列表
        this.bulletlist = {};

        //捕到的鱼
        this.capture = {};
        //打死的鱼
        this.fishdie = {};
        //奖励ID
        this.bonusids = [];
        //用户场次id
        this.matchid;
    };

    /**
     * 取用户资料
     * */
    kioskmgr.prototype.get_bysession = function (session, callback) {
        var me = this;
        if (typeof (callback) != 'function') return;
        if (!common.validate_value(session, "len:32")) {
            return common.invokecallback(callback, new Error('get_bysession param error!'), null);
        }

        me.client.db.select("kiosk", "*", [{ kiosk_session: session }], null, 1, function (err, data) {
            if (err) {
                return common.invokecallback(callback,err, null);
            }
            var res = data[0];
            me.kiosk_session = res.kiosk_session;
            me.kiosk_id = res.kiosk_id;
            me.kiosk_status = res.kiosk_status;
            me.kiosk_agencyid = res.kiosk_agencyid;
            me.kiosk_username = res.kiosk_username;
            me.kiosk_balance_a = res.kiosk_balance_a;
            me.kiosk_balance_b = res.kiosk_balance_b;
            me.kiosk_balance_c = res.kiosk_balance_c;
            return common.invokecallback(callback, err, me);
        });
    };
    /**
     *取用户状态合约 
     **/
    kioskmgr.prototype.getstatus = function (kioskid, callback) {
        var me = this;
        if (typeof (callback) != 'function') return;
        if (!common.validate_value(kioskid, "number")) {
            return common.invokecallback(callback, new Error('getstatus param error!'), null);
        }
    
        me.client.db.select("kiosk", "*", [{ kiosk_id: kioskid }], null, 1, function (err, data) {
            var status = false;
            if (err) {
                return common.invokecallback(callback, err, false);
            }
                 
            if (data[0].kiosk_status===1) { 
                status = true;
            }
            return common.invokecallback(callback, err, status);
        });
    };
       
    
        /**
     * 用户下注
     * */
    kioskmgr.prototype.onBet = function (_bet) {
        var me = this;     
        if (me.kiosk_balance_a >= _bet) {
            me.kiosk_balance_a -= _bet;
            me.betstake = _bet;
            me.bettotal += _bet;
            return true;
        } else {
            return false;
        }
   };
    
        /**
     * 用户赢分
    **/
    kioskmgr.prototype.onWin = function (_win) {
        var me = this;
        me.kiosk_balance_a += _win;
        me.wintotal += _win;
    };
    
        /**
     * 检查余额
     * */
    kioskmgr.prototype.onbalance = function (bet) {
        var me = this;
        if (me.kiosk_balance_a >= bet) {
             return true;
        } else {
             return false;
        }
    }

    module.exports = function (client) {
        return new kioskmgr(client);
    };

})();

