(function () {
    var common = require('../library/common.js');
 
    function tablemgr(server) {
        this.server = server;
        this.table_id;
        this.table_agentid;
        this.table_gameid;
        this.table_ptype;
        this.table_maxkiosk;
        this.table_ip;
        this.table_port;
        this.table_updated;
        this.table_created;
    }

    tablemgr.prototype.gettabel = function (tableid,callback) {
        var me = this;      
        me.server.db.select('game_multiplayer_table', '*', [{ game_multiplayer_table_id: tableid }],null,1, function (err, res) {
            if (err) {
                return common.invokecallback(callback, err, null);
            } else {
                var data = res[0];
                me.table_id = data.game_multiplayer_table_id;
                me.table_agentid = data.game_multiplayer_table_agentid;
                me.table_gameid = data.game_multiplayer_table_gameid;
                me.table_ptype = data.game_multiplayer_table_ptype;
                me.table_maxkiosk = data.game_multiplayer_table_maxkiosk;
                me.table_ip = data.game_multiplayer_table_ip;
                me.table_port = data.game_multiplayer_table_port;
                me.table_updated = data.game_multiplayer_table_updated;
                me.table_created = data.game_multiplayer_table_created;

                return common.invokecallback(callback, err, me);
            }
        });
    }
    module.exports = function (server) {
        return new tablemgr(server);
    }
})();