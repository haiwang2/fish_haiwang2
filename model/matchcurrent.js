(function () {
    var common = require('../library/common.js');
    /**
    *用户场次统计
    */
    function matchcurrent(client) {
        this.client = client;
    }

    matchcurrent.prototype.result = function (result, gameid, gamematchid,callback) {
        var me = this;
        if (common.empty(result)) {
            me.client.error("error", "match_current_error", "match current param error");
            return common.invokecallback(callback, new Error('matchresult result param error!'), false);
        }
        var params = {
            kioskid: result.kiosk_id,
            agentid: result.kiosk_agencyid,
            gameid: gameid,
            gamematchid:gamematchid,
            matchid: result.matchid,
            bettotal:result.bettotal,
            winamt:result.wintotal,
            result: result.fishdie,
            created: "{DB_NOW}"
        }
        me.client.db.insert('match_haiwang2_current', params, function (err, data) {
            if (err) {
                me.client.error("error", "match_current_error", err);
                return common.invokecallback(callback, err, false);
            }
            return common.invokecallback(callback, err, true);
        });
    }

    module.exports = function (client) {
        return new matchcurrent(client);
    }

})();