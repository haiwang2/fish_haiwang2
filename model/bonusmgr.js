(function () {
   var bonustypelist = require('../config/bonus.json'),
       fishbonus = require('../config/fishbonus.json'),
       common = require('../library/common.js');

  function bonusmgr(){}

  
  bonusmgr.prototype.createbonus = function(bonustype){
        if (bonustype == 0) {
            return null;
        }
        var bonusproto = bonustypelist[bonustype];
        var bonus = {
            type: bonusproto.bonustype,
            interval: bonusproto.interval,
        };

        if (undefined === bonus.interval)
            bonus.interval = 1000000;

        bonus.number = 1;
        if (!!bonusproto.probability) {
            bonus.number = parseInt(common.rndweightitem(bonusproto.probability));
        }
        switch (bonus.type) {
            case 22:
                bonus.boom = [0];
                for (var i = 0 ; i < bonus.number - 1; i++) {
                    if (i == 0) {
                        var k = common.random(1, 4);
                        bonus.boom.push(k);
                    } else {
                        var k = common.random(1, 3);
                        if (k == bonus.boom[i]) {
                            bonus.boom.push(4);
                        } else {
                            bonus.boom.push(k);
                        }
                    }
                }
                break;
            default:
                break;
        }
        return bonus;
    }

    /**
     * ������������
     * @param fishtype
     * @returns {number}
     */
  bonusmgr.prototype.createbonustype = function (fishtype) {
        var bonustype = 0;
        switch (fishtype) {
            case 8:
                bonustype = common.rndweightitem(fishbonus['8']);
                break;
            default:
                bonustype = common.randomprobabilityarr(fishbonus['*']);
                if (!bonustype) {
                    bonustype = 0;
                }
                break;
        }
        return bonustype;
    }

    module.exports = new bonusmgr();

})();

