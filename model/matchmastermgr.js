﻿
(function () {
    var common = require('../library/common.js');
    function matchmgr (client) {
        this.client = client;
        this.matchid=0;
    }
    /**
     * 保存用户场次
     * */
    matchmgr.prototype.openmatch = function (obj,gameid, callback) {
        var me = this;
        if (common.empty(obj)|| common.empty(gameid)) {
            me.client.error("error", "open_match_invalid", "open match param invalid");
            return common.invokecallback(callback, new Error('open_match_param_invalid'), false);
        }

        var param = {
            kioskid:obj.kiosk_id, 
            kiosktype:'ck', 
            gameid:gameid, 
            bonusid:0, 
            mtype:'N', 
            ptype:'pointa', 
            openbal: obj.kiosk_balance_a, 
            bettotal: obj.bettotal, 
            winamt: obj.wintotal, 
            payout:0, 
            refund:0, 
            balance: obj.kiosk_balance_a, 
            state:'play', 
            jtype:0, 
            jamt:0, 
            updated: '{DB_NOW}', 
            created: '{DB_NOW}'
        };

        me.client.db.insert("match_master_current",param, function (err, data) {
            if (err) {
                me.client.error("error", "open_match_invalid", err);
                return common.invokecallback(callback, err, false);
            }
            obj.matchid = data.insertId;
            me.matchid = data.insertId;
            obj.gameid = obj.client.params.gameid;
            return common.invokecallback(callback, err, true);
        });
    }
    
    /**
     * update用户场次
     * */
    matchmgr.prototype.closematch = function (obj,gameid, state,callback) {
        var me = this;
        if (common.empty(obj)|| common.empty(gameid)) {
            me.client.error('error', 'param_invalid', 'param invalid : obj or gameid');
            return common.invokecallback(callback, new Error('param_invalid'), false);
        }

        var params = {
            bettotal:obj.bettotal,
            winamt:obj.wintotal,
            balance:obj.kiosk_balance_a,
            //capture:obj.capture,
            //fishdie:obj.fishdie,
            bonusid: 0 ,
            state: state,
            updated:'{DB_NOW}'
        }

        var condition=[
            {gameid:gameid,
            id:obj.matchid,
            kioskid:obj.kiosk_id}
        ];

        me.client.db.update("match_master_current",params,condition,function (err, data) {
            if (err) {
                me.client.error('error', 'param_invalid', err);
                return common.invokecallback(callback, err, false);
            }
            if (data.affectedRows > 0) 
                return common.invokecallback(callback, err, true);
            else
                return common.invokecallback(callback, err, false);
            
        });
    }
    
    module.exports = function (client) {
        return new matchmgr(client);
    }
})();

