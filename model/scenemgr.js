﻿(function () {
    /**
     * 场景类
     * */
    var common = require('../library/common.js');
    
    function scenemgr(server) {
        this.server = server;
        this.scenelist = {};
        this.scenelength = 0;
        
        this.scene_id;//场景id
        this.fish_rule;//场景出鱼的规则
        this.max_fish;//鱼的个数
        this.time;//场景时间
        this.group;//是否为组
        this.free;//是否免费
        this.ex_json;//其它
        this.syncwait;//同步等待时间
        this.begintime = 0;//场景开始时间
        this.msperframe = 40;//一幀的时长
        this.frame = 0;//场景跑了多少帧
        this.timeelapse = 0;//当前场景移过去时间 (秒数）
        this.timeremain = 0;//场景剩余时间 (秒数）   this.time - this.timeelapse  
        this.type = 0;
        this.scene_index =-1;//场景序号
        this.winamt = 0;//本场中奖总数
        this.bettotal = 0;//本场下注总数
        this.matchid = 0;//场次ID
        this.betdetail = {};//本场的死鱼情况[{"1":20,"win":90},{"2":20,"win":90}]
        this.result = {};//本场结果[{"1":120},{"2":150}]
    }
        
    /**
     * 初始化场景
     * */
    scenemgr.prototype.initlist = function (callback) {
        var me = this;
        if (typeof (callback) != 'function') return;

        me.server.db.select('game_haiwang2_scene', '*', function (err, result) {
            if (err) { 
                return callback(err, null);
            }
            for (var i = 0; i < result.length; i++) {
                var res = result[i];
                var scene = {
                    scene_id: res.scene_id,
                    fish_rule: JSON.parse(res.scene_fish_rule),
                    max_fish: res.scene_max_fish,
                    time: res.scene_time,
                    group: res.scene_group,
                    free: res.scene_free,
                    ex_json: res.scene_ex_json,
                    syncwait: res.scene_syncwait,
                    scene_ex_json: 0,
                    msperframe: 40,
                    frame: 0,
                    timeelapse: 0,
                    timeremain: 0,
                    type: 0,
                    scene_index: i,
                    winamt: 0,
                    bettotal: 0,
                    matchid: 0,
                    begintime: 0,
                    betdetail: {},
                    result:{}
                }
                me.scenelist[i] = scene;
            }
            me.scenelength = result.length;
            return callback(err, me.scenelist[0]);
        });
    }
   
    /**
     * 切换场景
     * @param sceneid
     * @returns {*}
     */
    scenemgr.prototype.next = function (sceneindex) {
        var me = this;
        //根据sceneid查询场景信息
        var nextsceneid = ++sceneindex % me.scenelength;
        var scene = me.scenelist[nextsceneid];
        scene.begintime = new Date().getTime();

        me.begintime = scene.begintime;
        me.bettotal = scene.bettotal;
        me.frame = scene.frame;
        me.matchid = scene.matchid;
        me.msperframe = scene.msperframe;
        me.scene_index = scene.scene_index;
        //me.scenelength = scene.scenelength;
        me.timeelapse = scene.timeelapse;
        me.timeremain = scene.timeremain;
        me.type = scene.type;
        me.winamt = scene.winamt;
        me.time = scene.time;
        me.scene_id = scene.scene_id;
        me.fish_rule = scene.fish_rule;
        me.max_fish = scene.max_fish;
        me.group = scene.group;
        me.free = scene.free;
        me.ex_json = scene.ex_json;
        me.syncwait = scene.syncwait;
        betdetail= {},
        result={}
        return scene;
    };
    
    /**
     * 退出当前场景换场景
     * @param sceneid
     * @returns {boolean}
     */
    scenemgr.prototype.timeout = function () {
        //开始时与当前时间比，如果大于等time则return true表示切换场景;
        var me = this;
        var nowtime = new Date().getTime();
        var differtime = (nowtime - me.begintime) / 1000;
        me.timeelapse = differtime;
        if (differtime > me.time) {
            return true;
        } else
            return false;
    };
    
    module.exports = function (server) {
        return new scenemgr(server);
    }
})();

