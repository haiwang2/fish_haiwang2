﻿(function () {
    var common = require('../library/common.js');
    
    /**
 * 用户流水帐
 * */
var trxcurrentmgr = function (client) {
        this.client = client;
    }
    
    /**
 * 保存用户流水帐
 * */ 
trxcurrentmgr.prototype.trxcurrent = function (obj, callback) {
        var me = this;
        if (!common.validate({
            kiosk_trx_kioskid: "must" , 
            kiosk_trx_open_balance: 'must',
            kiosk_trx_wallet_type: 'must',
            kiosk_trx_amt: 'must',
            kiosk_trx_balance: 'must',
            kiosk_trx_gameid: 'must',
            kiosk_trx_matchid: 'must'
        }, obj)) {
            return callback(new Error('param error!'), null);
        }

        var param = {
            kiosk_trx_agencyid: obj.kiosk_trx_agencyid,
            kiosk_trx_kioskid: obj.kiosk_trx_kioskid,
            kiosk_trx_open_balance: obj.kiosk_trx_open_balance,
            kiosk_trx_wallet_type: obj.kiosk_trx_wallet_type,
            kiosk_trx_amt: obj.kiosk_trx_amt,
            kiosk_trx_balance: obj.kiosk_trx_balance,
            kiosk_trx_gameid: obj.kiosk_trx_gameid,
            kiosk_trx_game_desc: '',
            kiosk_trx_gsn:0,
            kiosk_trx_matchid: obj.kiosk_trx_matchid
        }
        me.client.db.insert('kiosk_trx_current', param,function (err, data) {
            if (err) {
                return callback(err, null);
            }
            return callback(err, data);
        });  
    }
    
    /**
     * 取得流水数据模型
     * */
    trxcurrentmgr.prototype.gettrx = function (kisokobj, gameid,bet) {
        return trx = {
            kiosk_trx_status: kisokobj.kiosk_status,
            kiosk_trx_agencyid: kisokobj.kiosk_agencyid,
            kiosk_trx_kioskid: kisokobj.kiosk_id,
            kiosk_trx_type: 0,
            kiosk_trx_open_balance: kisokobj.kiosk_balance_a,
            kiosk_trx_wallet_type: 'pointa',
            kiosk_trx_amt: bet,
            kiosk_trx_balance: kisokobj.kiosk_balance_a,
            kiosk_trx_gameid: gameid,
            kiosk_trx_game_desc:'fishhaiwang2',
            kiosk_trx_jptype:0,
            kiosk_trx_matchid: kisokobj.matchid,
            kiosk_trx_gsn:"",
            kiosk_trx_trxid:0,
            kiosk_trx_device:0,
            kiosk_trx_refund:0,
            kiosk_trx_created_by:0,
            kiosk_trx_created_date: '{DB_NOW}'
        };
    }

    module.exports = function (client) {
        return new trxcurrentmgr(client);
    }
})();

