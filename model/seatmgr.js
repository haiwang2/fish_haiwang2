(function () {
    var common = require('../library/common.js');

    function seatmgr (server) {
        this.server = server;
        this.seatlist=[];
    }

    /**
     * 服务器初始化获得该桌子下的所有座位的列表
     * @param tableid
     * @param callback
     */
    seatmgr.prototype.initseat = function (tableid , callback) {
        var me = this;
        if (common.empty(tableid)) {
            me.server.debug('initseat params is  null');
            return common.invokecallback(callback, new Error('param_invalid'), null);
        }
        me.server.db.select('game_multiplayer_seat', ['game_multiplayer_seat_id'], [{game_multiplayer_seat_tableid: tableid }], function (err, res) {
            if (err) {
                return common.invokecallback(callback, err, null);
            } else {
                for (var id in res) {
                    me.seatlist.push(res[id].game_multiplayer_seat_id);
                }
                return common.invokecallback(callback, err, res);
            }
        });
    }
        
    /**
     * 用户坐下与离开
     * @param seatid
     * @param userid
     * @param state
     * @param callback
     */
    seatmgr.prototype.joinandqiut = function (obj, callback) {
        var me = this;
        if (common.empty(obj.gameid) ||common.empty(obj.tableid) || common.empty(obj.seatid) || common.empty(obj.userid) || common.empty(obj.state)) {
            me.server.debug('joinandqiut params is  null');
            return common.invokecallback(callback, new Error('param_invalid'), null);
        }
        var param = {
            game_multiplayer_seat_state :obj.state,
            game_multiplayer_seat_userid : obj.userid,
            game_multiplayer_seat_updated : '{DB_NOW}',
        };
        var condition = {
            game_multiplayer_seat_id : obj.seatid,
            game_multiplayer_seat_tableid: obj.tableid,
            game_multiplayer_seat_userid: obj.userid,
            game_multiplayer_seat_state: obj.state
        };

        me.server.db.update('game_multiplayer_seat',param, condition, function (err, res) {
            if (err) {
                return common.invokecallback(callback, err, false);
            }
            if (res.affectedRows < 1) {
                return common.invokecallback(callback, 'joinandqiut' + state + 'not data', false);
            }
            return common.invokecallback(callback, err, true);
        });
    }

    module.exports = function (server) {
        return new seatmgr(server);
    }
})();




