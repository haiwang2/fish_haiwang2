(function () {
    var common = require('../library/common.js');
    /**
     *游戏场次统计 
     **/
    function matchresult(client) { 
        this.client = client;
    }

    matchresult.prototype.result = function (result, gameid, gamemacthid, callback) {
        var me = this;
        if (common.empty(result)) {
            me.client.error('error', 'match_result_error', 'matchresult result param error!');
            return common.invokecallback(callback, new Error('matchresult result param error!'), false);
        }
        var params = {
            kioskid:result.kiosk_id, 
            agentid: result.kiosk_agencyid, 
            gameid: gameid, 
            matchid: result.matchid, 
            result: result.fishdie, 
            created:"{DB_NOW}"
        }
        me.client.db.insert('match_result_current', params, function (err, data) {
            if (err) {
                me.client.error('error', 'match_result_error', err);
                return common.invokecallback(callback, err, false);
            }
            return common.invokecallback(callback, err, true);
        });        
    }

    module.exports = function (client) {
        return new matchresult(client);
    }
})();
