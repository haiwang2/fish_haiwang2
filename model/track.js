(function () {
    var track = function (opts) {
        this.distance = opts.distance;
        this.track_name = opts.track_name;
        this.type = opts.type;
        this.starttime = new Date().getTime();
        this.frame = 0;
    }
    /**
    * 计算是否超时
    * @param nowtime
    * @returns {boolean}
    */
    track.prototype.timeout = function (nowtime) {
        var me = this;
        //算出当前跑了帧数       
        var time = nowtime - me.starttime;
        me.frame = Math.floor(time * 25 / 1000);
        //这里需要跟据鱼的速度来计算，鱼的速度是随机的[10,50][0]
        if (me.frame < me.distance) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 更新当前帧数
     */
    track.prototype.nextframe = function (nowtime) {
        var time = nowtime - me.starttime;
        me.frame = Math.floor(time * 25 / 1000);
    }

    module.exports = track;
})();
