(function () {
    var common = require('../library/common.js');

    /**
     *游戏管理 
     **/
    function gamemgr(server) {
        this.server = server;
        this.game_id=1;
        this.game_status;
        this.game_name_eng;
        this.game_type;
        this.game_minbet;
        this.game_maxbet;
        this.game_winratio;
        this.game_payline;
    }
    /**
     *初始化游戏设定  目前只做了从mysql取
     **/
    gamemgr.prototype.init = function (callback) {
        var me = this;
        me.server.db.select("game", "*",[{game_id:55}], function (err, data) {
            if (err) {
                return common.invokecallback(callback, err, null);
            }
            var res = data[0];
            me.game_id= res.game_id;
            me.game_status=res.game_status;
            me.game_name_eng=res.game_name_eng;
            me.game_type=res.game_type;
            me.game_minbet= res.game_minbet;
            me.game_maxbet=res.game_maxbet;
            me.game_winratio=res.game_winratio;
            me.game_payline=res.game_payline;
            return common.invokecallback(callback, err, me);
        });
    }


    module.exports = function (server) {
        return new gamemgr(server);
    }

})();
