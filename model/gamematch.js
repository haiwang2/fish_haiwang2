﻿(function () {
    var common = require('../library/common.js');

    function gamematchmgr(server) {
        this.server = server;
        this.matchid=0;
    }
     /**
      *新增游戏场次 
      **/
    gamematchmgr.prototype.newmatch = function (gameid,tableid,callback) {
        var me = this;
        if (common.empty(gameid) || common.empty(tableid)) {
            me.server.debug('new game match params is  null');
            return common.invokecallback(callback, new Error('param_invalid'), null);
        }
        var param = {
            game_multiplayer_match_tableid : tableid,
            game_multiplayer_match_gameid : gameid,
            game_multiplayer_match_kiosckcount : 0,
            game_multiplayer_match_time_matchstart : "{DB_NOW}",
            game_multiplayer_match_time_matchend : "{DB_NOW}",
            game_multiplayer_match_state : "working",
            game_multiplayer_match_bettotal : 0,
            game_multiplayer_match_wintotal :0,
            game_multiplayer_match_betdetail :'',
            game_multiplayer_match_result :'',
            game_multiplayer_match_updated:"{DB_NOW}"
        };
        me.server.db.insert('game_multiplayer_match',param, function (err, res) {
            if (err) {
                return common.invokecallback(callback, err, false);
            }
            
            me.matchid = res.insertId;
            return common.invokecallback(callback, null,true);
        });
    }
    /**
     *更新游戏场次 
     **/
    gamematchmgr.prototype.updatematch = function (matchobj,callback) { 
        var me = this;
        if (common.empty(matchobj)) {
            me.server.debug('update game match params is  null');
            return common.invokecallback(callback, new Error('param_invalid'), false);
        }
        var param = {
            game_multiplayer_match_kiosckcount: matchobj.userlist,
            game_multiplayer_match_time_matchend : "{DB_NOW}",
            game_multiplayer_match_state: matchobj.state,
            game_multiplayer_match_bettotal: matchobj.bettotal,
            game_multiplayer_match_wintotal: matchobj.wintotal,
            game_multiplayer_match_betdetail: matchobj.betdetail,
            game_multiplayer_match_result: matchobj.result,
            game_multiplayer_match_updated: "{DB_NOW}"
        };
        var condition = [
             { game_multiplayer_match_tableid: matchobj.tableid },
             { game_multiplayer_match_gameid: matchobj.gameid },
             { game_multiplayer_match_id: matchobj.matchid }
        ]
        me.server.db.update('game_multiplayer_match', param, condition, function (err, res) {
            if (err) {
                return common.invokecallback(callback, err, false);
            }           
            return common.invokecallback(callback, null, true);
        });
    }   
    
    module.exports = function (server) {
        return new gamematchmgr(server);
    }

})();

