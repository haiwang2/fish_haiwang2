(function () {
    var common = require('../library/common.js'),
        track = require('../model/trackmgr.js');

    /**
    *鱼管理类
    */
    function fish(opts) {
        //this.fish_id=opts.fish_id; //鱼id
        this.type_id = opts.id;//鱼类型id
        this.fish_type = opts.type;       
        this.fish_odds=opts.win_bonus;//倍率
        this.fish_speed=opts.speed;//鱼的速度
        this.fish_probility = opts.win_ratio;//中奖率
        this.fish_born_ratio =opts.born_ratio //出生机率
        this.fish_send_fps = opts.fish_send_fps;//多久发一次一鱼
        this.cat = opts.cat;
        this.track;//鱼轨迹
        this.frame = 0;//鱼在此轨迹跑了多少帧
        //最后一次派鱼时间
        this.last_new_fish = 0;
        this.bonustype = 0;
    }
    

    /**
     *检查鱼是否存在屏内
     */
    fish.prototype.timeout = function (cunTime) {
        var me = this;
        //如果游出场景，移除鱼
        var timeout = me.track.timeout(cunTime);
        me.frame = me.track.frame;  //这里需要跟据鱼的速度来计算，鱼的速度是随机的[10,50][0]
        return timeout;
    }

    /**
     *获得不同类型鱼的倍率
     */
    fish.prototype.getodds = function () {
        var me = this;
        var fish_odds = JSON.parse(me.fish_odds);                 
        return common.random(fish_odds[0], fish_odds[1]);
    }

    module.exports =fish;
   
})();
