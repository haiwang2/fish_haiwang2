
/*
* index.js 
* 初始化和启动游戏的主体文件，现阶段属于初期开发阶段。
* 基本这个档案，我(YK)会全权处理，各位只看如何处理，不要修改。
* 底层封装待处理
* - 网络通讯类
*/

//(function(){
	var env = 'default', //将从环境变数中取得。
		gamecode = 'fishing_haiwang2', //将从环境变数中取得，以后会有更多其他不同游戏，所以这里交由环境变数让伺服器知晓在执行什么游戏。
		common = require('./library/common'),
		//Retrive Config 获取设定，全部设定就放这里吧。
		config = require('./config/config.json'),
		//创建server对象
		server = require('./library/server')(config[env], config[env].serverport),
		//创建主游戏控制对象
		game = require('./game/' + gamecode.split("_")[0] + '/' + gamecode)(server),
		//创建用户列表
		clients = {};
    server.net.on('connection', function (socket) {
       
            console.log("connection  ok!");
		    //创建用户
		    var client = require('./library/client')(socket, server.cluster);

		    //用户登入，办理登入手续
		    //if(game.userjoin(client)){
		
		    //用户退出，办理登出手续
		    client.socket.on('disconnect', function () {
			    common.invokecallback(game.userquit,client);
			    delete server.clients[client.socket.id];
		    });
			
		    //客户端传回的各项资料从这跑向控制对象game主游戏类。
		    //游戏自动route进去游戏类的某个方法。
            client.socket.on('route', function (event, data){
                client.params = data;
                console.log('==========route===============');
			    common.invokecallback(game, game[event],client, data);
            });
        
            client.socket.on('userjoin', function (event, data) {
                client.params = data;
                console.log('==========route===============');
			    //game.userjoin(client, data);
            });

		    //登记到用户列表
		    server.clients[client.socket.id] = client;

		    //}else{
		    //	//用户被拒接，踢出局。
		    //	client.socket.disconnect();
		    //	delete client;
		    //}
	});
//})();