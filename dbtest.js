
//db 测试


var env = 'default',
common = require('./library/common'),
config = require('./config/config.json'),
cluster = require('./library/dbcluster')(config[env]),
db = require('./library/db')(cluster),

async = require('async');
//db.select('abc_a', '*', ['{DB_LB}',{id:"{DB_LE}10"}], function(err, result){
//	console.log(db.lastquery);
//	console.log(result);
//});
//db.select("kiosk", "*", [{ kiosk_session: 1 }], null, 1, function (err, data) {
//    console.log(data);
    
//});


async.waterfall([    
    function (next) {
        db.begin(function (err, data) {
            console.log("begin", err);
            next(err, data);
        });
    }, function (res, next) {
        db.update('game_haiwang2_scene', { scene_group: 99 }, [{ scene_id: 1 }], function (err, data) {
            console.log("update", err);
            next(err, data);
        });
    }, function (res, next) {
        db.insert('game_haiwang2_scene', { scene_fish_rule: [[1, 50], [2, 10], [3, 5], [4, 5], [5, 5], [6, 10]], scene_max_fish: 50, scene_time: 1023, scene_group: 0, scene_free: 0, scene_ex_json:null, scene_syncwait: 40 }, function (err, data) {
            console.log("insert",err);
            next(err, data);
        });
    }, function (res, next) {
        db.commit(function (err, data) {
            console.log("commit",err);
            next(err, data);
        });
    }
], function (err, res) {
    if (err) {
        db.rollback();
        db.destroy();
    }
});


