﻿(function(){

    var common = require('../library/common.js'),
		async = require('async');

	/**
	 * 用户管理类，注意，只是写个调试样本而已，并未测试，需要继续处理。
	 * */
    function kiosk(client){
        this.client = client;
        this.model = { //要用到的资料库模型列表对象
			agent : require('../model/agentmgr.js')(client), 
			kioskmgr : require('../model/kioskmgr.js')(client), 
            trx: require('../model/trxmgr.js')(client),
            matchmgr: require('../model/matchmastermgr.js')(client),
            matchcurrent: require('../model/matchcurrent.js')(client),
            matchresult: require('../model/matchresult.js')(client)
		}
	}

	//用session载入用户
	kiosk.prototype.load = function (callback) {
		var me = this;

		if (!common.validate_value(me.client.params.session, "min:1")){
			me.client.error('error', 'param_invalid', 'param invalid : session');
			return common.invokecallback(callback, new Error('param_invalid'), false);
		}

		async.waterfall([
			function(cb){
				me.model.kioskmgr.get_bysession(me.client.params.session, function (err, kiosk) {
					if (err)
						return cb(err, 'unknow_session');
					else{
						me.id = kiosk.kiosk_id;
						me.kiosk = kiosk;
						return cb(err,null);
					}
				});
			},
            function (res,cb){
				me.model.agent.get(me.kiosk.kiosk_agencyid, function (err, agent) {
					if (err)
						return cb(err, 'agent_failed');
					else{
						me.agent = agent;
						return cb(null, 'ok');
					}
				});
			},
		],function(err, status){
			switch(status){
				case 'param_invalid':
					me.client.error('error', 'param_invalid', 'param invalid : session');
				case 'unknow_session':
					me.client.error('error', 'unknow_session', 'Unknown Session Or Session Expired. Session: ' + me.client.params.session);
				case 'agent_failed':
					me.client.error('error', 'unexpected_error', 'Unable Retrive Kiosk Agency Data on Kiosk_Retrive.');
				case 'ok':
					return common.invokecallback(callback, null, true);
            }
            if (err) { 
                me.client.error('error', 'unexpected_error', err);                
            }
            return common.invokecallback(callback, err, false);
		});
	};

	/**
	 * 检查用户是否合约（合法）
	 * */
	kiosk.prototype.status = function (callback) {
		var me = this;
		if (common.empty(me.kiosk) || common.empty(me.agent)){
			me.client.error('error', 'unexpected_error', 'Unable empty kiosk or agent on kiosk.status');
			return common.invokecallback(callback, new Error('unexpected_error'), false);
		}
		async.waterfall([
			function(cb){
                me.model.kioskmgr.getstatus(me.id, function (err, status) {
					if (err)
						return cb(err, false);
					else
						return cb(null, status);
				});
			},
			function(status,cb){
				if(!status)
					return cb(new Error('kiosk status is false'), status);
				else
                    me.model.agent.getstatus(me.agent.agency_upline_structure, function (err, status) {                        
						if (err)
							return cb(err, false);
						else
							return cb(null, status);
					});
			}
		],function(err, status){
			if(err)
				me.client.error('error', 'unexpected_error', err);
			return common.invokecallback(callback, err, status);
		});
	};
    
    /**
     *写用户流水，用户发射，和死fish时
     **/
    kiosk.prototype.newtrx = function (trxobj,callback) {
        var me = this;
        if (common.empty(trxobj)) {
            me.client.error('error', 'unexpected_error', 'Unable empty trxobj on kiosk.newtrx');
            return common.invokecallback(callback, new Error('unexpected_error'), false);
        }
        //写用户流水       
        me.model.kioskmgr.newtrx(trxobj, function (err, trx) {
            if (err)
                return common.invokecallback(callback, err, false);
            else
                return common.invokecallback(callback, err, true);
        });        
    };
    
    kiosk.prototype.transaction = function (kioskobj, gameid, gamematchid,state, callback){
		var me = this;
        if (common.empty(kioskobj) || common.empty(gameid) || common.empty(state)) {
            me.client.error('error', 'unexpected_error', 'Unable empty kiosk or gameid and state is null');
            return common.invokecallback(callback, new Error('unexpected_error'), false);
        }
        async.waterfall([
            function (next) {//关闭场次
                me.model.matchmgr.closematch(kioskobj, gameid, state, function (err, data) {
                    if (err) {
                        next(err, 'updatematch_failed');
                    }
                    next(null, data);
                });
            }, function (res, next) {//保存用户统计结果
                if (res) {
                    me.model.matchcurrent.result(kioskobj, gameid, gamematchid, function (err, data) {
                        if (err) {
                            next(err, 'match_current_result_failed');
                        }
                        next(err, data);
                    });
                } else {
                    next(null, null);
                }
            }, function (res, next) {//保存场次统计结果
                if (res) {
                    me.model.matchresult.result(kioskobj, gameid, gamematchid, function (err, data) {
                        if (err) {
                            return next(err, "match_result_failed");
                        }
                        return next(err, data);
                    });
                } else {
                    next(null, null);
                }
            },
            function (res, next) {//开场
                me.model.matchmgr.openmatch(kioskobj, gameid, function (err, data) {
                    if (err) {
                        next(err, 'newmatch_failed');
                    }
                    next(err, 'ok');
                });
            }
        ], function (err, data) {
            switch (data) {
                case "updatematch_failed":
                    me.client.error('error', 'unexpected_error', 'updatematch failed');
                    break;
                case "newmatch_failed":
                    me.client.error('error', 'unexpected_error', 'newmatch failed');
                    break;
                case "match_current_result_failed":
                    me.client.error('error', 'match_current_result_failed', 'match current result failed');
                    break;
                case "match_result_failed":
                    me.client.error('error', 'match_result_failed', 'match result failed');
                    break;
                case "ok":
                    return common.invokecallback(callback, null, true);
                    break;
            }
            if (err) { 
                me.client.error('error', 'unexpected_error', err);                
            }
            return common.invokecallback(callback, new Error('unexpected_error'), false);
        });
	};
    
    ///**
    // *写用户场次信息
    // **/
    //match.prototype.newmatchmgr = function (kiosk, gameid, callback) {
    //    var me = this;
    //    if (common.empty(kiosk) || common.empty(gameid)) {
    //        me.client.error('error', 'unexpected_error', 'Unable empty kiosk');
    //        return common.invokecallback(callback, new Error('unexpected_error'), false);
    //    }
    //    me.model.matchmgr.openmatch(kiosk, gameid, function (err, data) {
    //        if (err) {
    //            me.client.error('error', 'unexpected_error', err);
    //            return common.invokecallback(callback, err, false);
    //        }
    //        return common.invokecallback(callback, err, true);
    //    });
    //}
    
    
    ///**
    // *更新户场次信息
    // **/
    //match.prototype.updatematchmgr = function (kiosk, gameid, state, callback) {
    //    var me = this;
    //    if (common.empty(kiosk) || common.empty(gameid)) {
    //        me.client.error('error', 'unexpected_error', 'Unable empty kiosk');
    //        return common.invokecallback(callback, new Error('unexpected_error'), false);
    //    }
    //    me.model.matchmgr.closematch(kiosk, gameid, state, function (err, data) {
    //        if (err) {
    //            me.client.error('error', 'unexpected_error', 'Unable empty kiosk');
    //            return common.invokecallback(callback, err, false);
    //        } else {
    //            return common.invokecallback(callback, err, true);
    //        }
    //    });
    //}

	module.exports = function(client){
		return new kiosk(client);
	};

})();