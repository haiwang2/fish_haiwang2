// JavaScript source code
(function () {
    var common = require('../library/common'),
        async = require('async');

    function match(server){
        this.server = server;
        this.model = {
            gamematch: require('../model/gamematch.js')(server),
            //matchmgr: require('../model/matchmastermgr.js')(server),
            //matchcurrent: require('../model/matchcurrent.js')(server),
            //matchresult: require('../model/matchresult.js')(server)
        };
    }
    /**
     *增加游戏场次 
     **/
    match.prototype.newmatch = function (match,callback){
        var me = this;
        if (common.empty(kiosk)) {
            me.server.debug('error', 'param_invalid', 'param invalid : kiosk');
            return common.invokecallback(callback, new Error('param_invalid'), false);
        }
        me.model.gamematch.newmatch(match, function (err, data) {
            if (err) {
                return common.invokecallback(callback, err, false);
            }
            return common.invokecallback(callback, err,true);          
        });
    }
    
    /**
     *更新游戏场次 
     **/
    match.prototype.updatamatch = function (match, callback) {
        var me = this;
        if (common.empty(kiosk)) {
            me.server.debug('error', 'param_invalid', 'param invalid : kiosk');
            return common.invokecallback(callback, new Error('param_invalid'), false);
        }
        me.model.gamematch.updatematch(match, function (err, data) {
            if (err) {
                return common.invokecallback(callback, err, false);
            }            
            return callback(err,true);
        });
    }
    /**
     *游戏场次事务处理 
     **/
    match.prototype.transaction = function (match, callback) {
        var me = this;
        if (common.empty(match)) {
            me.server.debug('error', 'param_invalid', 'param invalid : kiosk');
            return common.invokecallback(callback, new Error('param_invalid'), false);
        }
        async.waterfall([
            function (next) {//更新本场次信息
                me.model.gamematch.updatematch(match, function (err, data) {
                    if (err) {
                        return next(err, "update_error");
                    }
                    return next(err, data);
                });
            }, 
            function (res, next){//写新场次
                me.model.gamematch.newmatch(match.gameid, match.tableid, function (err, data) {
                    if (err) {
                        return next(err, "newmatch_error");
                    }
                    return next(err,'ok');
                });
            }
        ], function (err, data) {
            console.log("transaction  end", err);
            switch (data) {
                case "update_error":
                    me.server.debug('error', 'update_error', err);
                    return common.invokecallback(callback, err, false);
                    break;
                case "newmatch_error":
                    me.server.debug('error', 'newmatch_error', err);
                    return common.invokecallback(callback, err, false);
                    break;
                case "match_result_error":
                    me.server.debug('error', 'match_result_error', err);
                    return common.invokecallback(callback, err, false);
                    break;
                case "ok":
                    return common.invokecallback(callback, null, true);
                    break;
            }
        });
    }
    
    ///**
    // *写用户场次信息
    // **/
    //match.prototype.newmatchmgr = function (kiosk,gameid, callback) {
    //    var me = this;
    //    if (common.empty(kiosk) || common.empty(gameid)) {
    //        me.server.debug('error', 'unexpected_error', 'Unable empty kiosk');
    //        return common.invokecallback(callback, new Error('unexpected_error'), false);
    //    }
    //    me.model.matchmgr.newmatch(kiosk,gameid, function (err, data) {
    //        if (err) {
    //            return common.invokecallback(callback, err, false);
    //        }
    //        return common.invokecallback(callback, err, true);
    //    });
    //}
    
    
    ///**
    // *更新户场次信息
    // **/
    //match.prototype.updatematchmgr = function (kiosk,gameid,state,callback) {
    //    var me = this;
    //    if (common.empty(kiosk)|| common.empty(gameid)) {
    //        me.server.debug('error', 'unexpected_error', 'Unable empty kiosk');
    //        return common.invokecallback(callback, new Error('unexpected_error'), false);
    //    }
    //    me.model.matchmgr.updatematch(kiosk,gameid,state, function (err, data) {
    //        if (err) {
    //            return common.invokecallback(callback, err, false);
    //        } else {
    //            return common.invokecallback(callback, err, true);
    //        }
    //    });
    //}
    
    /**
     * 用户场次事务
     **/
    //match.prototype.matchtmgrransaction = function (kiosk,gameid, state,callback) {
    //    var me = this;
    //    if (common.empty(kiosk)|| common.empty(gameid)||common.empty(state)) {
    //        me.server.debug('error', 'unexpected_error', 'Unable empty kiosk or gameid and state is null');
    //        return common.invokecallback(callback, new Error('unexpected_error'), false);
    //    }
    //    async.waterfall([
    //        function (next) {
    //            me.model.matchmgr.closematch(kiosk,gameid, state, function (err, data) {
    //                if (err) {
    //                    next(err, 'updatematch_failed');
    //                }       
    //                next(null, data);
    //            });
    //        }, function (res, next) {//保存用户统计结果
    //            if (res) {
    //                me.model.matchcurrent.result(kiosk, function (err, data) {
    //                    if (err) {
    //                        next(err, 'match_current_result_failed');
    //                    }
    //                    next(err, data);
    //                });
    //            } else {
    //                next(null, null);
    //            }
    //        }, function (res, next) {//保存场次统计结果
    //            if (res) {
    //                me.model.matchresult.result(kiosk, me.model.gamematch.matchid, function (err, data) {
    //                    if (err) {
    //                        return next(err, "match_result_error");
    //                    }
    //                    return next(err, data);
    //                });
    //            } else { 
    //                next(null, null);
    //            }
    //        },
    //        function (res, next) {
    //            me.model.matchmgr.openmatch(kiosk,gameid, function (err, data) {
    //                if (err) {
    //                    next(err, 'newmatch_failed');
    //                }                    
    //                next(err, 'ok');
    //            });
    //        }
    //    ], function (err, data) {
    //        switch (data) {
    //            case "updatematch_failed":
    //                me.server.debug('error', 'unexpected_error', err);
    //                return common.invokecallback(callback, new Error('unexpected_error'), false);
    //                break;
    //            case "newmatch_failed":
    //                me.server.debug('error', 'unexpected_error', err);
    //                return common.invokecallback(callback, new Error('unexpected_error'), false);
    //                break;
    //            case "match_current_result_failed":
    //                me.server.debug('error', 'match_current_result_failed', err);
    //                return common.invokecallback(callback, new Error('unexpected_error'), false);
    //                break;
    //            case "ok":
    //                return common.invokecallback(callback, null, true);
    //                break;
    //        }       
    //    });
    //};
    
    module.exports = function (server) {         
        return new match(server);
    }
})();