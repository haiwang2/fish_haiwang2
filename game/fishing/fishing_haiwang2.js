(function(){	
	//这个类是控制类，也是游戏的逻辑所在。
	//游戏大部分的类，会在这里载入和使用。

    var common = require('../../library/common'),
        async = require('async'),
        config = {}, //创建空config对象，待导入
        server = {}, //创建空server对象，待导入
        db = {}, //创建空db对象，待导入
        timerfish = require('../../library/timer'),
        timersence = require('../../library/timer');       
        
		//在这里载入各个游戏的模块来处理游戏的逻辑

    function game(server) {
        this.server = server;
        this.model = {
            setting: require('../../model/gamesetting.js')(this.server),
            game: require('../../model/gamemgr.js')(this.server),
            seat: require('../../model/seatmgr.js')(this.server),    
            scene: require('../../model/scenemgr.js')(this.server),           
            fish: require('../../model/fishmgr.js')(this.server),            
            match: require('../../module/match.js')(this.server),
            bonusmgr: require('../../model/bonusmgr.js'),
            trackmgr:require('../../model/trackmgr.js')
        };
        this.tableid = 1;
        this.currentmatchid = 0;
        this.fishid = 0;//鱼id     
        this.fishlist = {};//鱼表列表
        this.scenetimefalg = true;
        this.newfishfalg = true;
        this.testcount = 0;
        this.init();
    }
    /**
     *游戏初始化 
     **/
    game.prototype.init = function () {
        var me = this;
        async.waterfall([
            function (next) {
                me.model.game.init(function (err, data) {
                    if (err) {
                        return next(err, 'init_game_param_err');
                    }
                    return next(err,data);
                });
            }, function (res, next){ //注册timer事件
                timersence.on("stop", function () { return me.nextsence() });
                timerfish.on('tick', function () { return me.newfish() });
                next(null, 'ok');
            }
        ], function (err, res) {
            switch (res) {
                case "init_game_param_err":
                    me.server.debug('init game param err!');
                    break;
                case "ok":
                    me.nextsence(); //启动场次
                    break;
            }           
        });
    }

	//各个介入点和逻辑计算的方法
    game.prototype.userjoin = function(client,data){
        var me = this;
        var retmsg = { status: 'ok', error: '' };
        //参数合法性
        if (!common.validate({
                session: "must", 
                gameid: "must",
                tableid: "must"
            }, data)) {
                client.error('error', 'userjoin_params', "userjoin params error");            
                return;
            };
        
        var session = data.session;
        var gameid = data.gameid;
        var tableid = data.tableid;
              
        async.waterfall([
            function (next) {
                //用户进入游戏
                client.kiosk.load(function (err, status) {
                    if (err) {
                        return next(err, 'load_failed');
                    }else {                       
                        return next(null, data);
                    }
                });
            },
            function (res, next) { 
                //检查合法
                client.kiosk.status(function (err, status) {
                    if (err) {
                        return next(err, 'status_failed');
                    } else {
                        return next(null, data);
                    }
                });
            }, function (res, next){
                //桌子检测坐位初始化
                if (tableid != me.tableid) {//这里待确认配置tableid
                    return next(new Error('tableid exist'), 'tableid_exist');
                }
                if (me.model.seat.seatlist.length === 0) {
                    me.model.seat.initseat(tableid, function (err, data) {
                        if (err) {
                            return next(err, "init_seat_failed");
                        }
                        return next(err, data);
                    });
                } else {
                    return next(null, res);
                }
            },
            function (res, next) {
                //分配坐位
                client.kiosk.model.kioskmgr.seat_id = me.model.seat.seatlist.sort().shift();
                var obj = {
                    gameid:gameid,
                    tableid:tableid,
                    seatid: client.kiosk.model.kioskmgr.seat_id,
                    userid: client.kiosk.model.kioskmgr.kiosk_id,
                    state:'occupancy'
                }
                me.model.seat.joinandqiut(obj, function (err, data) {
                   if (err) {
                        me.model.seat.saetlist.push(client.kiosk.seat_id);
                        return next(err, 'set_seat_failed');
                    }
                    return next(err, null);
                });
            }, function (res, next) {
                //写场次信息            
                client.kiosk.model.matchmgr.openmatch(client.kiosk.model.kioskmgr,gameid, function (err, data) { 
                    if (err) {
                        return next(err, 'new_match_failed');
                    }
                    return next(err, 'ok');
                });
            }
        ], function (err, res) {
            switch (res) {
                case "load_failed":
                    client.error('error', 'load_failed', err);                
                    break;
                case "status_failed":
                    client.error('error', 'status_failed', err);                
                    break;
                case "init_seat_failed":
                    client.error('error', 'init_seat_failed', err);                   
                    break;
                case "set_seat_failed":
                    client.error('error', 'set_seat_failed', err);                   
                    break;
                case "tableid_exist":
                    client.error('error', 'tableid_exist', err);
                    break;
                case "ok":
                    retmsg.seatid = client.kiosk.model.kioskmgr.seat_id;
                    //用户列表
                    retmsg.users = [];
                    var index = 0;
                    for (var id in me.server.clients) {
                        var user = {
                            id: me.server.clients[id].kiosk.model.kioskmgr.kiosk_id,
                            name: me.server.clients[id].kiosk.model.kioskmgr.kiosk_username,
                            point: me.server.clients[id].kiosk.model.kioskmgr.kiosk_balance_a,
                            seatid: me.server.clients[id].kiosk.model.kioskmgr.seat_id
                        };
                        retmsg.users[index] = user;
                        index++;
                    }
                    //场景
                    retmsg.scene = {
                        id: me.model.scene.scene_id,
                        type: me.model.scene.type, 
                        msperframe: me.model.scene.msperframe,
                        frame: me.model.scene.frame,
                        timeelapse: me.model.scene.timeelapse,
                        timeremain: me.model.scene.time - me.model.scene.timeelapse ,
                        syncwait: me.model.scene.syncwait
                    }
                    //游戏配置  
                    retmsg.game = {
                        id: me.model.setting.gameid,
                        minbet: me.model.setting.minbet,
                        maxbet: me.model.setting.maxbet,
                        maxbullet: me.model.setting.maxbullet
                    };
                    //鱼列表
                    retmsg.fishes = [];
                    index = 0;
                    for (var id in me.model.fish.fishlist) {
                        retmsg.fishs[i].fishid = me.model.fish.fishlist[id].fishid,
                        retmsg.fishs[i].fishtype = me.model.fish.fishlist[id].fishtype,
                        retmsg.fishs[i].track = me.model.fish.fishlist[id].track.track＿name,
                        retmsg.fishs[i].frame = me.model.fish.fishlist[id].frame,
                        retmsg.fishs[i].bonustype = me.model.fish.fishlist[id].bonustype
                        index++;
                    }
                    //回复消息
                    client.respone("userjoin",retmsg);
                    var broadcast_msg = {
                        id: client.kiosk.model.kioskmgr.kiosk_id,
                        name: client.kiosk.model.kioskmgr.kiosk_username,
                        point: client.kiosk.model.kioskmgr.kiosk_balance_a,
                        seatid: client.kiosk.model.kioskmgr.seat_id
                    };
                    //推送有人新人加入
                    client.broadcast("userjoin", broadcast_msg);
                    //标记用户登录入
                    client.setverify(true);
                    break;
            }           
            client.output('userjoin', 'broadcast_userjoin');
        });
	};
    
    //match_result_current 游戏的结果
    //用户总帐统计
    game.prototype.userquit = function (client){
        var me = this; 
        if (client.getverify()) {
            var retmsg = { status: 'ok', error: '' };
            retmsg.seatid = client.kiosk.model.kioskmgr.seat_id;
            retmsg.userid = client.kiosk.model.kioskmgr.kiosk_id;
            //更新用户场次
            //统计用户场次总帐
            client.kiosk.modle.matchmgr.closematch(client.kiosk.model.kioskmgr, me.model.game.game_id,'done', function (err, data) {
                if (err) {
                    client.error('error', 'user_quit_error', err);
                    return;
                }
                
                if (retmsg.seatid > -1) {
                    client.broadcast('userquit', retmsg);
                    client.kiosk.model.kioskmgr = {};
                }                
                client.setverify(false);
            });            
        }
	};
    /**
     * 用户换枪 
     **/
    game.prototype.changebet = function (client, data){
        //参数合法性
        if (!common.validate({
            betstake: "must", 
            guntype: "must"
        }, data)) {
            console.log("param error!");
            return;
        }
		var me = this;
        var ret_msg = { status: 'ok', error: null };        
        //检查字弹值是否有效
        if (data.betstake >= me.model.game.game_minbet && data.betstake <= me.model.game.game_maxbet) {
            client.kiosk.model.kioskmgr.betstake = data.betstake;
            client.kiosk.model.kioskmgr.guntype = data.guntype;
        }                
        //返回用户换枪状态
        client.respone("changebet",ret_msg);
        var broadcast_msg = {
            seatid: client.kiosk.model.kioskmgr.seat_id,
            betstake: client.params.betstake,
            guntype: client.params.guntype
        }
        //广播用户换枪
        client.broadcast('broadcast_changebet', broadcast_msg);
        client.output('changebet','broadcast_changebet');
	};
    
    /**
     * 发射子弹
     * */
	game.prototype.shooting = function(client,data){
        var me = this;
        var bulletlist = client.kiosk.model.kioskmgr.bulletlist;
        var seatid = client.kiosk.model.kioskmgr.seat_id;
        var bet = parseInt(client.params.betstake);
        var ret_msg = {status:"ok",error:""};
        var broadcast_msg = {};
        
        async.waterfall([
            function (next) {//用户合法检查 
                client.kiosk.status(function (err, status) {
                    if (err) {
                        return next(err, 'status_failed');
                    } 
                    if(status){
                        return next(null, 'kiosk_invalid');
                    }
                    return next(null, status);
                });
            },
            function (res, next) {
                if (bet < me.model.game.game_minbet && bet > me.model.game.game_maxbet) {
                    client.error('error', 'param_invalid', 'betstake invalid '+ bet);
                    return next(new Error('betstake invalid' + bet), 'betstake_invalid');
                }
                if (bulletlist[bet] === undefined) {
                    bulletlist[bet] = 0;
                }
                //子弹列表
                bulletlist[bet]++;
                //本场用户下注
                me.model.scene.bettotal += bet;

                //交易处理    
                if (me.model.scene.free > 0) {//免费

                } else if (client.kiosk.model.kioskmgr.onBet(bet) == false) {
                    ret_msg.status = 'error';
                    ret_msg.error = 'point limit';
                    return next('point limit', 'point_limit');
                }
                ret_msg.point = client.kiosk.model.kioskmgr.kiosk_balance_a;
                ret_msg.seatid = client.kiosk.model.kioskmgr.seat_id;
               
                broadcast_msg.point = client.kiosk.model.kioskmgr.kiosk_balance_a;
                broadcast_msg.seatid = client.kiosk.model.kioskmgr.seat_id;
                broadcast_msg.betstake = bet;
                broadcast_msg.shotangle = client.params.shotangle;
                next(null, null);
            },
            function (res, next){//记录用户流水
                var trx = client.kiosk.model.trx.gettrx(client.kiosk.model.kioskmgr, me.model.game.game_id, "-" +bet);
                client.kiosk.model.trx.trxcurrent(trx, function (err, data) {
                    if (err) { return next(err, 'trx_err'); }
                    return next(err, 'ok');
                });
            }
        ], function (err, res) {
            switch (res) {
                case "point_limit":
                    client.error('error', 'point_limit', 'point limit');
                    break;
                case "trx_err":
                    client.error('error', 'trx_err', 'trx err');
                    break;
                case "betstake_invalid":
                    client.error('error', 'betstake_invalid', 'betstake invalid ');
                    break;
                case "kiosk_invalid":
                    client.error('error', 'koisk_invalid', 'kiosk invalid ');
                    break;
                case "ok":
                    client.respone('shooting', ret_msg);
                    client.broadcast('broadcast_shooting', broadcast_msg);
                    client.output('shooting', 'broadcast_shooting');
                    break;
             }
        });		
	};
    
    /**
     *捕鱼 
     ***/
	game.prototype.fishcapture = function(client,data){
		var me = this;
        var bet = parseInt(client.params.betstake);
        var bulletid = client.params.bulletid;
        var fisheids = JSON.parse(client.params.fishes);
        var winfishs = {};
        var totalwin = 0;
        var seatid = client.params.seatid;
        var bulletlist = client.kiosk.model.kioskmgr.bulletlist;
        var bonus = null;
        var retmsg = {status: 'ok',error: null,};
        
        async.waterfall([
        //子弹管理和奖励管理
            function (next){
                //移除子弹            
                if (bulletlist[bet] > 0) {
                    bulletlist[bet]--;
                } else {
                    var user_bonus = client.kiosk.model.kioskmgr['bonus'];
                    if (user_bonus === undefined) {
                        //报错
                        return next("user bonus is undefined", 'bonus_is_undefined');
                    }
                    //是否到到时间
                    var pass_time = (new Date().getTime() - user_bonus.begin) / 1000;
                    if (pass_time > user_bonus.interval || user_bonus.number <= 0) {
                        //过期了                    
                        delete client.kiosk.model.kioskmgr['bonus'];
                        return next("user : " + client.kiosk.id + " fish capture bonus pass time:" + pass_time + " ,user_bonus.interval :" + user_bonus.interval + " , user_bonus.number:" + user_bonus.number, 'bullet_is_null');
                    }
                    user_bonus.number--;
                    bet = user_bonus.bet;
                }
                return next(null, null);
            }, function (res, next) {//检测死鱼和中奖及交易
                var currTime = new Date().getTime();
                for (var id in me.fishlist) {
                    var fish = me.fishlist[id];
                    if (fish.timeout(currTime)) {
                        delete me.fishlist[id];
                        me.model.fish.fishtypecount[fish.type_id]--;
                    } 
                }

                for (var i = 0; i < fisheids.length; i++) {
                    var fishid = fisheids[i];
                    var fish = me.fishlist[fishid];
                    if (fish === undefined) {
                        continue;
                    }
                    
                    //用户捕的鱼
                    client.kiosk.model.kioskmgr.capture[fish.fish_type]++;
                    //风险 算出死鱼的总金额，检测风险
                    var tempwin = fish.getodds() * bet;
                    
                    //并产生bouns
                    fish.bonustype = 22;
                    //是否带bonus
                    if (fish.bonustype > 0) {
                        bonus = me.model.bonusmgr.createbonus(fish.bonustype);
                        if (bonus) {
                            //当前 用户绑定bonus
                            bonus['bet'] = parseInt(bet);
                            bonus['begin'] = new Date().getTime();
                            client.kiosk.model.kioskmgr['bonus'] = bonus;
                        }
                        switch (fish.bonustype) {
                            case 21://闪电连锁
                                var fishcount = commons.rndweightitem(bonus.probability);
                                bonus['fishs'] = me.filterfishtype(fishcount, [1, 3, 5]);
                                break;
                            case 22://连环炸弹蟹
                                var fishcount = commons.rndweightitem(bonus.probability);
                                bonus['fishs'] = me.filterfishtype(fishcount, [1, 3, 5]);
                                break;
                        }
                    }

                    if (me.chekwindbw(tempwin)) {
                        //判断是否死
                        if (common.random(0, 1)) {
                            //将死鱼信息加入winfishs 
                            var winamt = fish.getodds() * bet;
                            totalwin += winamt;
                            
                            
                            //将打死的鱼记录起来
                            if (winfishs[fish.fish_id] === undefined) {
                                winfishs[fish.fish_id] = 0;
                            }
                            winfishs[fish.fish_id] = winamt;
                            //本场用户总中奖
                            me.model.scene.winamt += winamt;
                            //fishtype:1,count:20,win:500,bet:10
                            //用户捕获后列死的鱼中奖
                            client.kiosk.model.kioskmgr.fishdie[fish.fish_type] += winamt;
                            me.model.scene.result[fish.fish_type] += winamt;
                            
                            //移除死了的鱼
                            for (var id in me.fishlist) {
                                if (id === fish.fish_id) {
                                    delete me.fishlist[id];
                                }
                            }
                        }
                    }
                }
                //交易处理　
                if (totalwin > 0) {
                    client.kiosk.model.kioskmgr.onWin(totalwin);
                }                
                return next(null, null);
            },function (res,next) {//记录用户流水             
                var trx = client.kiosk.model.trx.gettrx(client.kiosk.model.kioskmgr, me.model.game.game_id, totalwin);
                client.kiosk.model.trx.trxcurrent(trx, function (err, data) {
                    if (err) { return next(err, 'trx_err'); }
                    return next(err,data);
                });
            },function (res,next) {//回复消息处理
                retmsg = {
                    status: 'ok',
                    error: null,
                    point: client.kiosk.model.kioskmgr.kiosk_balance_a,
                    seatid: client.kiosk.model.kioskmgr.seat_id,
                    totalwin: totalwin,
                    fishes: winfishs
                };
                if (bonus) {
                    retmsg.bonus = bonus;                   
                }
                return next(null, 'ok');
            }
        ],
        function (err, res) {
            switch (res) {
                case "trx_err":
                    client.error('error', 'trx_err', err);
                    break;
                case "bullet_is_null":
                    client.error('error', 'bullet_is_null', err);
                    break;
                case "bonus_is_undefined":
                    client.error('error', 'bonus_is_undefined', err);
                    break;
                case "ok":
                    client.respone('fishcapture', retmsg);
                    client.broadcast('broadcast_fishdie', retmsg);
                    client.output('fishcapture', 'broadcast_fishdie');
                    break;
            }            
        });
	};
    
    /**
     *发送新鱼 
     **/
	game.prototype.newfish = function(){
        var me = this;
        var broadcast_msg = {};
        if (!me.newfishfalg) {
            me.server.debug('上一次还未完成...');
            me.newfishfalg = true;
            return;
        }
        me.newfishfalg = false;
        if (me.model.scene.scene_index === -1) {
            me.server.debug('没有场景，发新鱼这样好吗...');
            me.newfishfalg = true;
            return;
        }
        //{“鱼的种类”：[同时出现，场景中出现个数,场景时间开始出，场景时间必须出]
        // { "1": [2, 20, -1, 2], "2": [1, 10], "3": [1, 10], "4":  [1,10],"5": [1,10],"6":  [1,10]}
        var rule = me.model.scene.fish_rule;
        var scene_max_fish = me.model.scene.max_fish;
        if (!rule) {
            me.server.debug('没有场景规则，我没办法发新鱼哦...');
            me.newfishfalg = true;
            return;
        }
        //检查一下有没有时间过期的鱼
        var currTime = new Date().getTime();
        var index = 0;
        for (var id in me.fishlist) {
            var fish = me.fishlist[id];
            if (fish.timeout(currTime)) {
                delete me.fishlist[id];
                me.model.fish.fishtypecount[fish.type_id]--;
            } else {
                //记录活鱼的个数
                index++; 
            }
        }
        //从场景规则中拿出鱼的出生概率
        var fishsrule = {}; //{"1":20,"2":30,"4":60}    
        var timerule = {}; 
        for (var id in rule) {
            var temp = rule[id];
            if(temp.length>=2){
                fishsrule[id] = temp[1];                
            }
            if (temp.length == 4) {
                timerule[id] = temp[2];
            }
        }
        if (!fishsrule) {
            me.server.debug('场景规则中没找到鱼的概率，我没办法发新鱼哦...');
            me.newfishfalg = true;
            return;
        }
        //根据概率取可发的鱼类
        var fishtype = common.rndweightitem(fishsrule); 
        //产生一条鱼
        var fish = me.model.fish.newfish(fishtype);
        if (fish === undefined) {
            me.server.debug('没取到可发送的鱼类型...');
            me.newfishfalg = true; 
            return;
        }
        //此类鱼在场景中的最大数
        fish.fishscenemax = fishsrule[fish.type_id];
        //此类鱼在场景中的可出现时间点和必出现时间点
        fish.tiemrule = timerule[fish.type_id];

        var nowtime = new Date().getTime();
        if (fish.last_new_fish === 0) fish.last_new_fish = new Date().getTime();
        var runtime = (nowtime - fish.last_new_fish) / 1000;
        
        //场景中的鱼数量，这类鱼的数量                
        if (index >= scene_max_fish) {
            me.server.debug('当前场景的鱼数量到达最大量了...');
            me.newfishfalg = true; 
            return;
        }
        
        if (me.model.fish.fishtypecount[fishtype] <= fish.fishscenemax) {
            me.server.debug('此类型鱼在场景中名额已满...');
            me.newfishfalg = true; 
            return;      
        }

        if (fish.tiemrule != undefined) {
           if (fish.tiemrule > me.model.scene.timeelapse) {
                me.server.debug('此类型鱼在场景中还没到出生时间...');
                me.newfishfalg = true; 
                return;
           }
        }
        
        //计时
        fish.last_new_fish = new Date().getTime();
        //鱼的轨迹
        fish.track = me.model.trackmgr.getonlytrack(fishtype);
        //鱼的奖励类型 需要控制同一种bouns出现的次数
        fish.bonustype = me.model.bonusmgr.createbonustype(fishtype);
        fish.fish_id = me.fishid++;
        //加入鱼列表
        me.fishlist[fish.fish_id] = fish;
        broadcast_msg = {
            fishid: fish.fish_id,
            fishtype: fishtype, 
            track: fish.track.track_name,
            bonustype : fish.bonustype
        };
        me.server.broadcast('broadcast_newfish', broadcast_msg);
        me.server.output('broadcast_newfish');
        console.log('new fish....', broadcast_msg);

        if (fish.fishscenemax != -1) {
            if (me.model.fish.fishtypecount[fishtype] === undefined) {
                me.model.fish.fishtypecount[fishtype] = 0;
            }
            me.model.fish.fishtypecount[fishtype]++;
        }
        me.newfishfalg = true;          
    }
    
    /**
     *更换场景 
     **/
	game.prototype.nextsence = function (){
        var me = this;
        var scenetimer = 0;
        async.waterfall([
            function (next) {
                //检测场景是否初始化
                console.log("检测场景是否初始化");
                if (me.model.scene.scenelength === 0) {
                    me.model.scene.initlist(function (err, data) {
                        if (!err && common.empty(data)) {
                            err = new Error('scene not data');
                        }
                        if (err) { return next(err, null); }                       
                        return next(null, 'init');
                    });
                } else {                    
                    return next(null, me.model.scene);
                }
            }, function (res, next){
                //检测场景是否过期
                console.log("检测场景是否过期");
                async.waterfall([
                    function (next) { //初始化场景
                        var scene = me.model.scene.next(me.model.scene.scene_index);
                        scenetimer = scene.time;
                        console.log("=====","===scene_id="+scene.scene_id+"===index==="+scene.scene_index+"===scene"+ scene)
                        return next(null, scene);
                    }, function (res, next){//开启事务
                        me.server.db.begin(function (err, data) {
                            if (err) {                                   
                                server.debug(err);
                                return next(err, data);
                            }                               
                            return next(err, data);                                 
                        });
                    }, function (res, next){ //处理用户场次
                        var clients = [];
                        var userlist = me.server.getusers();
                        var index = 0;
                       
                        for (var id in userlist) {
                            userlist[id].db.dbc = me.server.db.dbc;
                            clients.push(userlist[id]);
                            index++;
                        }
                        
                        if (index > 0) {
                            me.matchtransaction(clients, me.model.game.game_id, next);
                        } else {
                            return next(null,res);
                        }
                    }, function (res, next) { //处理游戏场次
                        if (!res)
                            return next(new Error('begin_error'), res);
                        var match = {
                            tableid: me.tableid,
                            gameid: me.model.game.game_id || '1',
                            userlist: 0, //待处理
                            state: "closed",
                            bettotal: me.model.scene.bettotal,
                            wintotal: me.model.scene.winamt,
                            betdetail: me.model.scene.betdetail,
                            result: me.model.scene.result,
                            matchid: me.model.match.model.gamematch.matchid
                        }
                        me.model.match.transaction(match, function (err, data) {
                            if (err) { return next(err, null); }
                            return next(err, true);
                        });
                    }, function (res, next) { //事务提交
                        me.server.db.commit(function (err) {
                            if (err) {
                                return next(err, false); 
                            }                            
                            return next(null,true);                                 
                        }); 
                    }
                ], function (err, data) {
                    if (err) {
                        me.server.db.rollback();
                    }
                    me.server.db.destroy();
                    var broadcastmsg = {
                        id:me.model.scene.scene_id,
                        type:me.model.scene.type,
                        syncwait:me.model.scene.syncwait
                    }
                    me.fishlist = {};//清空上一场景的鱼儿
                    me.model.fish.fishtypecount = {}; //清空上一场景发的鱼儿计数器
                    me.fishid = 0;
                    me.server.broadcast("broadcast_newmatch", broadcastmsg);
                    me.server.output("broadcast_newmatch");
                    timersence.start(scenetimer);
                    me.newfish();                    
                    return next(null, 'ok');
              });
            }
        ], function (err, res) {
            if (!err && common.empty(res)) {
                err = new Error('scene is null');
            }
            if (err) {
                me.server.debug(err);
                console.log("check scene error", err);
                return;
            }
            console.log("check scene.....", me.testcount++);            
        });    
	};
    
    /**
     * 递归处理各用户的场次信息
     **/
    game.prototype.matchtransaction = function (clients,gameid, callback) {
        var me = this;
        clients[0].kiosk.transaction(clients[0].kiosk.model.kioskmgr,gameid,me.model.match.model.gamematch.matchid,'done', function (err, data) {
            if (err) { 
                return callback(err, null);
            }
            clients.shift();
            if (clients.length > 0) {
                me.matchtransaction(clients,gameid, callback);
            } else {
                return callback(null, data);
            }
        });
    }
    
    /**
     *取指定类型的鱼 
     **/
    game.prototype.filterfishtype = function (count, filterarr){
        var me = this;
        var retarr = [];
        var allarr = [];
        for (var id in me.fishlist) {            
            if (filterarr.indexOf(me.fishlist[id].fish_type) >= 0) {
                allarr.push(me.fishlist[id]);
            }
        }
        for (var i = 0; i < count; i++) {
            if (i <= allarr.length - 1) {
                retarr.push(allarr[common.random(0, allarr.length - 1)]);
            }
        }
        return retarr;
    }

    module.exports = function(parentobj, configobj){		
		//导入config对象入
		config = configobj;		
		//导入server对象。
		server = parentobj;		
		//游戏自身要用到的资料库对象，server那里我拿掉了，没必要。
		db = require('../../library/db')(server.cluster);			
        return new game(server);       
	};

})();