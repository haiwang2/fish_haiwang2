/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50617
Source Host           : 127.0.0.1:3306
Source Database       : stargame_bigtwo

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2015-08-06 14:51:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for game_multiplayer_table
-- ----------------------------
DROP TABLE IF EXISTS `game_multiplayer_table`;
CREATE TABLE `game_multiplayer_table` (
  `game_multiplayer_table_id` bigint(20) NOT NULL,
  `game_multiplayer_table_agentid` bigint(20) NOT NULL,
  `game_multiplayer_table_gameid` varchar(100) NOT NULL,
  `game_multiplayer_table_ptype` enum('pointc','pointb','pointa') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pointa',
  `game_multiplayer_table_maxkiosk` int(10) NOT NULL,
  `game_multiplayer_table_ip` varchar(30) NOT NULL,
  `game_multiplayer_table_port` int(10) NOT NULL,
  `game_multiplayer_table_updated` datetime NOT NULL,
  `game_multiplayer_table_created` datetime NOT NULL,
  PRIMARY KEY (`game_multiplayer_table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
