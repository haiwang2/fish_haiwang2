/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50617
Source Host           : 127.0.0.1:3306
Source Database       : stargame_bigtwo

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2015-08-07 14:19:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for game_multiplayer_match
-- ----------------------------
DROP TABLE IF EXISTS `game_multiplayer_match`;
CREATE TABLE `game_multiplayer_match` (
  `game_multiplayer_match_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `game_multiplayer_match_tableid` bigint(20) NOT NULL,
  `game_multiplayer_match_gameid` bigint(20) NOT NULL,
  `game_multiplayer_match_kiosckcount` int(10) unsigned NOT NULL DEFAULT '0',
  `game_multiplayer_match_time_matchstart` datetime NOT NULL,
  `game_multiplayer_match_time_matchend` datetime DEFAULT NULL,
  `game_multiplayer_match_state` enum('working','closed','cancle') NOT NULL DEFAULT 'working',
  `game_multiplayer_match_bettotal` decimal(20,4) NOT NULL,
  `game_multiplayer_match_wintotal` decimal(20,4) NOT NULL,
  `game_multiplayer_match_betdetail` varchar(5000) NOT NULL,
  `game_multiplayer_match_result` varchar(5000) NOT NULL,
  `game_multiplayer_match_updated` datetime NOT NULL,
  PRIMARY KEY (`game_multiplayer_match_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
