(function(){
	
	var common = require('./common'),
		servereventemitter = new require('events').EventEmitter,
		clienteventemitter = new require('events').EventEmitter,
		util = require('util');
		
	function client(socket, server){
		clienteventemitter.call(this);

		var me = this,
			found = false;

		do{
			var id = common.gencode();
			if (server.clients.hasOwnProperty(id))
				found = true;
		}while(found);

		me.id = id;
		me.socket = socket;
		me.remote = {
			ip : socket.remoteAddress,
			port : socket.remotePort
		};
		me.server = server;
		me.received = '';
		me.sent = '';
        me.broadcasted = '';
        me.verify = false;
		me.socket.on('error', function(error){me.error(error, me)});
		me.socket.on('data', function(data){me.receive(data);});
		me.socket.on('end', function(){me.closed(false);});		
        me.socket.on('close', function (){ me.closed(true); });
        setTimeout(function () { return me.timeout() }, 10000);
	};
	util.inherits(client, clienteventemitter);

	client.prototype.receive = function(transmit){
		var data = {};
		this.received = transmit.toString();
		try{
			data = JSON.parse(this.received);
        } catch (e) {
            console.log(e);
        }
        if (data.hasOwnProperty('event') && data.hasOwnProperty('content')) {
            this.emit(data.event, data.content);
            this.emit('route', data.event, data.content);
        }
	};

	client.prototype.send = function(event, content){
		var transmit = '';
		try{
			transmit = JSON.stringify({event:event, content:content});
		}catch(e){}
		if(transmit !== ''){
			this.socket.write(transmit);
			this.sent = transmit;
		}
	};

	client.prototype.broadcast = function(event, content){
		var transmit = '';
		try{
			transmit = JSON.stringify({event:event, content:content});
		}catch(e){}
		if(transmit !== ''){
			for(var key in this.server.clients)
				if (key !== this.id)
					this.server.clients[key].socket.write(transmit);
			this.broadcasted = transmit;
		}
	};

	client.prototype.error = function(error, client){
		this.emit('socketerror', error.code, client);
	};

    client.prototype.closed = function (correct){
		delete this.server.clients[this.id];
		this.socket.destroy();
		this.emit('disconnect', correct);
	};

	client.prototype.disconnect = function(){
		delete this.server.clients[this.id];
		this.socket.destroy();
		this.emit('destroy', false);
    };
    
    client.prototype.timeout = function () {
        var me = this;
        if (!me.verify)
            me.socket.destroy();
    }
    

	function server(){
		servereventemitter.call(this);
		this.net = new require('net');
		this.created = false;
		this.clients = {};
		this.broadcasted = '';
	};
	util.inherits(server, servereventemitter);

	server.prototype.listen = function(port, ip){

		var me = this;

		// Start a TCP Server
		this.net.createServer(function (socket) {

			// Identify this client
			var newclient = new client(socket, me);

			// Put this new client in the list
			me.clients[newclient.id] = newclient;
			me.emit('connection', newclient);

		}).listen(port, ip);
		this.created = true;
	};

    server.prototype.broadcast = function (event, content){
		if(this.created === true && !common.empty(this.clients)){
			var transmit = '';
			try{
				transmit = JSON.stringify({event:event, content:content});
			}catch(e){}
			if(transmit !== '' && !common.empty(this.clients)){
                for (var key in this.clients) {
                    if(this.clients[key].verify)
                        this.clients[key].socket.write(transmit);
                }
				this.broadcasted = transmit;
			}
		}
	};

	server.prototype.close = function(callback){
		if(this.created === true){
			this.clients = {};
			this.net.close();
			common.invokecallback(callback);
		}
	};

    module.exports = new server;

})();