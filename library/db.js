(function () {
    
    var mysql = require('mysql'),
        async = require('async'),
        common = require('./common'),
        cluster = null,
	
        escape = function (value, auto, column) {
            var strescape = column === true ? mysql.escapeId(value) : mysql.escape(value);
            return auto === true ? strescape : strescape.substring(1, strescape.length - 1);
        },
	
        get_microtime = function () {
            return new Date().getTime();
        },
	
        get_elapsedms = function (start) {
            return (get_microtime() - parseFloat(start)) + ' ms';
        },
	
        get_colnm = function (dbcolnm, dbcola, alias, dbfnc) {
            var dbcolnow = '', 
                regex_db = /^\{DB_([A-Z_]+)\}/i,
                regex_col = /^'(.+)'$/, matches = [],
                regex_iscol = /^`(.)^$/ig;
            if (matches = regex_col.exec(dbcolnm))
                dbcolnow = escape(matches[1], true);
            else {
                if (typeof (dbcolnm) !== 'string')
                    dbcolnm = dbcolnm.toString();
                dbcolnm = dbcolnm.replace(/\./g, "`.`");
                if (matches = regex_db.exec(dbcolnm)) {
                    switch (matches[1].toUpperCase()) {
                        case 'DISTINCT':
                            dbcolnow = "DISTINCT " + get_colnm(dbcolnm.replace(matches[0], ""), null, false, false);
                            break;

                        case 'NO_CACHE':
                            $DBColNow = "SQL_NO_CACHE " + get_colnm(dbcolnm.replace(matches[0], ""), null, false, false);
                            break;

                        case 'HIGH_PRIORITY':
                            dbcolnow = "HIGH_PRIORITY " + get_colnm(dbcolnm.replace(matches[0], ""), null, false, false);
                            break;

                        case 'LOW_PRIORITY':
                            dbcolnow = "LOW_PRIORITY " + get_colnm(dbcolnm.replace(matches[0], ""), null, false, false);
                            break;

                        case 'FNC':
                            dbcolnow = get_colnm(dbcolnm.replace(matches[0], ""), null, false, true);
                            break;

                        case 'VAL':
                            dbcolnow = get_colval(dbcolnm.replace(matches[0], ""), dbcolnm);
                            break;

                        case "SUM":
                            dbcolnow = "SUM(" + get_colnm(dbcolnm.replace(matches[0], ""), null, false, false) + ")";
                            break;

                        case "COUNT":
                            dbcolnm = dbcolnm.replace(matches[0], "");
                            dbcolnow = dbcolnm === "*" || dbcolnm === "" ? "COUNT(*)" : "COUNT(" + get_colnm(dbcolnm, null, false, false) + ")";
                            break;

                        case "NOW":
                            dbcolnow = "NOW()";
                            break;

                        case common.inarray(matches[1], common.range('A', 'Z')):
                            dbcolnow = get_colnm(dbcolnm.replace(matches[0], ""), null, false, true);
                            break;
                    }
                } else if (dbfnc !== true && !regex_iscol.test(dbcolnm) && dbcolnm !== '*')
                    dbcolnow = escape(dbcolnm, true, true);
                else
                    dbcolnow = escape(dbcolnm);
                if (alias)
                    dbcolnow += (typeof (dbcola) === 'string' && !common.isnumeric(dbcola) && !common.isnumeric(dbcola) ? " AS " + escape(dbcola, true) : "");
            }
            return dbcolnow;
        },

        get_colval = function (dbcolval, dbcolnm) {
            if (dbcolval === null)
                return "NULL";
            else {
                if (typeof (dbcolval) === 'object' || common.isobj(dbcolval))
                    dbcolval = JSON.stringify(dbcolval);
                var regex_db = /^\{DB_([A-Z_]+)\}/i, matches = regex_db.exec(dbcolval);
                if (!common.empty(matches)) {
                    dbcolval = escape(dbcolval.replace(matches[0], ""));
                    switch (matches[1].toUpperCase()) {
                        case 'COL':
                            return get_colnm(dbcolval);

                        case 'FNC':
                            return dbcolval;

                        case "NOW":
                            return "NOW()";

                        case "SUM":
                        case "COUNT":
                            return get_colnm(dbcolval);

                        case "TIME":
                            return "CURRENT_TIME()";

                        case "DATE":
                            return "CURRENT_DATE()";

                        case "INC":
                            dbcolval = parseFloat(dbcolval);
                            return get_colnm(dbcolnm) + " + " + dbcolval;

                        case "DEC":
                            dbcolval = parseFloat(dbcolval);
                            return get_colnm(dbcolnm) + " + " + dbcolval;

                        case "TIMES":
                            dbcolval = parseFloat(dbcolval);
                            return get_colnm(dbcolnm) + " * " + dbcolval;

                        case "DIV":
                            dbcolval = parseFloat(dbcolval);
                            return get_colnm(dbcolnm) + " / " + dbcolval;

                        case "POWER":
                            dbcolval = parseFloat(dbcolval);
                            return get_colnm(dbcolnm) + " ^ " + dbcolval;
                    }
                } else
                    return escape(dbcolval, true);
            }
        },

        get_cond = function (dname, dvalue, having) {
            var strdbcond = '';
            switch (typeof (dvalue)) {
                case 'object':
                    var first = common.first(dvalue),
                        value = first['value'],
                        key = first['key'],
                        result = [];
                    strdbcond = get_colnm(dname, null, null, having);
                    switch (key.toUpperCase()) {
                        case "LIST":
                        case "IN":
                            if (typeof (value) === 'object' && !common.empty(value))
                                for (var vkey in value)
                                    result.push(escape(value[vkey]));
                            strdbcond += " IN ('" + common.implode("','", result) + "')";
                            break;

                        case "XLIST":
                        case "XIN":
                            if (typeof (value) === 'object' && !common.empty(value))
                                for (var vkey in value)
                                    result.push(escape(value[vkey]));
                            strdbcond += " NOT IN ('" + common.implode("','", result) + "')";
                            break;

                        case "BETWEEN":
                            value = common.array_values(value);
                            strdbcond += " BETWEEN " + escape(value[0], true) + " AND " + escape(value[1], true);
                            break;

                        case "XBETWEEN":
                            value = common.array_values($Value);
                            strdbcond += " NOT BETWEEN " + escape(value[0], true) + " AND " + escape(value[1], true);
                            break;

                        default:
                            strdbcond = "";
                    }
                    break;

                case 'number':
                case "string":
                    var regex_db = /^\{DB_([A-Z]+)\}/, operator = " = ", matches = [];
                    if (matches = regex_db.exec(dvalue)) {
                        switch (matches[1].toUpperCase()) {
                            case "NE":
                                operator = " != ";
                                break;
                            case "GE":
                                operator = " >= ";
                                break;
                            case "GT":
                                operator = " > ";
                                break;
                            case "LE":
                                operator = " <= ";
                                break;
                            case "LT":
                                operator = " < ";
                                break;
                            case "NNE":
                                operator = " <=> ";
                                break;
                            case "LIKE":
                                operator = " LIKE ";
                                break;
                            case "XLIKE":
                                operator = "NOT LIKE ";
                                break;
                            case "INULL":
                                operator = " IS NULL";
                                break;
                            case "XINULL":
                                operator = " IS NOT NULL";
                                break;
                        }
                        if (operator !== " = ")
                            dvalue = dvalue.replace(matches[0], "");
                    }
                    strdbcond = get_colnm(dname, null, null, having) + operator;
                    strdbcond += operator.substr(operator.length - 1) === " " ? get_colval(dvalue, dname) : "";
                    break;

                case 'NULL':
                    strdbcond = get_colnm(dname) + " = null";
            }
            return strdbcond;
        },

        get_condition = function (dbcond, having) {
            var sdbcond = '',
                regex_con = /(`?)([^\W]|[\w\d\-]+)(`?)(\s*)([\>=\<!]|LIKE|IS NOT NULL|IN|NOT IN|BETWEEN|NOT BETWEEN)(\s*)(('([^']*)')|[\d\.]|(\((.*)\))*)/ig;
            if (!common.empty(dbcond)) {
                if (dbcond !== null && !common.isarray(dbcond) && typeof (dbcond) === 'object')
                    dbcond = [dbcond];
                if (common.isarray(dbcond)) {
                    var dbscond = "",
                        regex_op = /^\{DB_(OR|XOR|LB|RB|AND)\}$/i,
                        logic = true,
                        braket = 0,
                        matches = [];
                    for (var i in dbcond) {
                        var key = '', value = '';
                        if (typeof (dbcond[i]) === 'string')
                            value = dbcond[i]
                        else if (typeof (dbcond[i]) === 'object' && !common.empty(dbcond[i])) {
                            var first = common.first(dbcond[i]);
                            key = first.key;
                            value = first.value;
                        } else
                            break;
                        matches = regex_op.exec(value);
                        if (typeof (value) === 'string' && !common.empty(matches)) {
                            switch (matches[1].toUpperCase()) {
                                case "OR":
                                    sdbcond += " OR ";
                                    logic = true;
                                    break;

                                case "XOR":
                                    sdbcond += " XOR ";
                                    logic = true;
                                    break;

                                case "LB":
                                    sdbcond += (!logic ? " AND " : "") + "(";
                                    braket++;
                                    break;

                                case "RB":
                                    if (braket > 0) {
                                        braket--;
                                        sdbcond += ")";
                                    }
                                    break;

                                default:
                                    sdbcond += " AND ";
                                    logic = true;
                                    break;
                            }
                        } else {
                            if (!common.empty(key)) {
                                sdbcond += (!logic ? " AND " : "") + get_cond(key, value, having);
                                logic = false;
                            }
                        }
                        isfirst = false;
                    }
                    sdbcond = sdbcond.replace(/(\s+)(AND|OR|XOR|\()(\s+)$/i, "");
                    if (braket > 0)
                        for (var i = braket; i > 0; i--)
                            sdbcond += ")";
                } else if (typeof (dbcond) === 'string' && regex_con.test(dbcond))
                    sdbcond = dbcond;
            }
            return !common.empty(sdbcond) ? (having ? " HAVING " : " WHERE ") + sdbcond : "";
        },

        get_colsql = function (dbcol, alias) {
            var sqlcol = "";
            if (typeof (alias) === 'undefined')
                alias = true;
            if (!common.empty(dbcol)) {
                switch (typeof (dbcol)) {
                    case 'object':
                        for (var colalies in dbcol)
                            sqlcol += (sqlcol !== "" ? ", " : "") + get_colnm(dbcol[colalies], colalies,
                            alias);
                        break;

                    case 'string':
                        sqlcol = get_colnm(dbcol, null, alias);
                }
            }
            return sqlcol;
        },

        get_group = function (group, having) {
            var sqlgroup = get_order(group, true);
            return !common.empty(sqlgroup) ? sqlgroup + get_having(having) : "";
        },

        get_having = function (having, dcol) {
            return !common.empty(having) ? get_condition(having, true) : "";
        },

        get_order = function (order, group) {
            var sqlorder = '';
            if (!common.empty(order)) {
                if (order !== null && !common.isarray(order) && typeof (order) === 'object')
                    order = [order];
                if (common.isarray(order)) {
                    var random = false;
                    for (var i in order) {
                        var oname = '', otype = '';
                        if (typeof (order[i]) === 'string') {
                            oname = order[i];
                            if (oname === "RAND()") {
                                otype = oname;
                                oname = '';
                            } else
                                otype = "ASC";
                        } else if (typeof (order[i]) === 'object' && !common.empty(order[i])) {
                            var first = common.first(order[i]);
                            oname = first.key;
                            otype = first.value.toUpperCase();
                            if (otype === '{DB_RAND}')
                                otype = "RAND()";
                            else if (otype !== "DESC" && otype !== "RAND()")
                                otype = "ASC";
                            if (otype === "RAND()")
                                oname = "";
                        } else
                            break;
                        if (oname !== "")
                            oname = get_colnm(oname);
                        sqlorder += (sqlorder !== '' ? ", " : "") + (oname !== '' ? oname + ' ' : '') + otype;
                        if (otype === "RAND()") {
                            random = true;
                            break;
                        }
                    }
                } else if (typeof (order) === 'string') {
                    if (order.toUpperCase() === "{DB_RAND}" || order.toUpperCase() === "RAND()") {
                        random = true;
                        sqlorder = "RAND()";
                    } else
                        sqlorder = get_colnm(order) + " ASC";
                }
                if (group === true && random === true && sqlorder !== '')
                    sqlorder = sqlorder.substring(0, sqlorder.length - 8);
            }
            return sqlorder !== '' ? (group === true ? " GROUP BY " : " ORDER BY ") + sqlorder : "";
        },

        get_limit = function (limit) {
            var sqllimit = '';
            if (!common.empty(limit)) {
                var offset = 0, limited = 0;
                switch (typeof (limit)) {
                    case "object":
                        if (limit !== null && !common.isarray(limit) && typeof (limit) === 'object')
                            limit = [limit];
                        if (common.isarray(limit)) {
                            limit = common.array_values(limit);
                            var item = 0;
                            limitobj:
                                if (limit.length > 1) {
                                    for (var i in limit) {
                                        switch (item) {
                                            case 0:
                                                offset = parseFloat(limit[i]);
                                                break;

                                            case 1:
                                                limited = parseFloat(limit[i]);
                                                break;

                                            default:
                                                break limitobj;
                                        }
                                        item++;
                                    }
                                } else
                                    limited = parseFloat(limit[0]);
                        } else {
                            if (limit.hasOwnProperty("offset") || limit.hasOwnProperty("OFFSET"))
                                offset = limit['offset'] || limit['OFFSET'];
                            if (limit.hasOwnProperty("limit") || limit.hasOwnProperty("LIMIT"))
                                limited = limit['limit'] || limit['LIMIT'];
                        }
                        break;

                    case "string":
                    case "number":
                    case "boolean":
                        limited = parseInt(limit, 10);
                        break;
                }
            }
            if (limited > 0)
                sqllimit += (sqllimit === '' ? '' : ' ') + "LIMIT " + limited;
            if (offset > 0)
                sqllimit += (sqllimit === '' ? '' : ' ') + "OFFSET " + offset;
            return ' ' + sqllimit;
        },
	
        get_tablesql = function (table) {
            if (typeof (table) === 'string' && table !== '') {
                table = table.replace(/\./g, "`.`");
                table = table.replace(/,/g, "`,`");
                return escape(table, true, true);
            } else
                return "";
        },

        get_deletesql = function (table, condition, order, limit) {
            var sqldata = '', sqlfields = '';
            for (var name in data) {
                sqlfields += (sqlfields !== '' ? ", " : "") + get_colnm(name)
                sqldata += (sqldata !== '' ? ", " : "") + get_colval(data[name], name);
            }
            return "DELETE FROM " + get_tablesql(table) + get_condition(condition) + get_order(order) + get_limit(limit);
        },

        get_insertsql = function (table, data) {
            var sqldata = '', sqlfields = '';
            for (var name in data) {
                sqlfields += (sqlfields !== '' ? ", " : "") + get_colnm(name)
                sqldata += (sqldata !== '' ? ", " : "") + get_colval(data[name], name);
            }
            return "INSERT INTO " + get_tablesql(table) + " (" + sqlfields + ") VALUES (" + sqldata + ")";
        },

        get_updatesql = function (table, data, condition, order, limit) {
            var sqlsetdata = '';
            for (var name in data)
                sqlsetdata += (sqlsetdata !== '' ? ", " : "") + get_colnm(name) + " = " + get_colval(data[name], name);
            return "UPDATE " + get_tablesql(table) + " SET " + sqlsetdata + get_condition(condition) +
				get_order(order) + get_limit(limit);
        },

        get_copysql = function (srctable, dsttable, data, condition, order, limit, group, having) {
            if (!common.empty(srctable) && !common.empty(dsttable) && !common.empty(data) && (typeof (data) === 'object' || data === '*')) {
                var colsrc = [], coldes = [];
                if (data === '*') {
                    return "INSERT INTO " + get_tablesql(dsttable) + " " + get_selectsql(srctable, "*", condition, order, limit, group, having);
                } else {
                    for (var name in data) {
                        colsrc.push(name);
                        coldes.push(data[name]);
                    }
                    var sqlcolsrc = get_colsql(colsrc, false);
                    return "INSERT INTO " + get_tablesql(dsttable) + "(" + sqlcolsrc + ") " +
					get_selectsql(srctable, coldes, condition, order, limit, group, having);
                }
            } else
                return false;
        },
	
        get_selectsql = function (table, column, condition, order, limit, group, having) {
            var sql = "SELECT " + get_colsql(column) + " FROM " + get_tablesql(table) +
			get_condition(condition) + get_group(group, having) +
			get_order(order) + get_limit(limit);
            console.log(sql);
            return "SELECT " + get_colsql(column) + " FROM " + get_tablesql(table) +
			get_condition(condition) + get_group(group, having) +
			get_order(order) + get_limit(limit);
        },
	
        get_paging = function (perpage, total, now) {
            perpage = parseInt(perpage, 10);
            total = parseInt(total, 10);
            now = parseInt(now, 10);
            if (total > perpage && now > 0) {
                var pgtotal = ((total - (total % perpage)) / perpage);
                pgtotal += ((total % perpage > 0) ? 1 : 0);
                if (now > pgtotal)
                    now = pgtotal;
                var begin = now * perpage;
                return { offset: now * perpage, limit: perpage };
            } else
                return { offset: 0, limit: 0 };
        },
	
        get_parameter = function (args, param) {
            var result = {}, ended = false;
            if (!common.empty(args)) {
                for (var i in args) {
                    if (typeof (args[i]) === 'function') {
                        result['callback'] = args[i];
                        ended = true;
                    } else {
                        if (ended === false)
                            result[param[i]] = args[i];
                        else
                            result[param[i]] = undefined;
                    }
                }
            } else {
                for (var i in param)
                    result[param[i]] = undefined;
            }
            return result;
        },
	
        get_connection = function (callback) {
            var me = this;
            if (common.empty(me.dbc)) {
                cluster.getconnection(function (err, connection) {
                    if (err)
                        common.invokecallback(callback, err, null, false);
                    else
                        common.invokecallback(callback, err, connection, true);
                });
            } else
                common.invokecallback(callback, null, me.dbc, true);
        },
	
        close_connection = function (dbc) {
            if (typeof (dbc) === 'object' && dbc !== null && typeof (dbc.release) === 'function' && typeof (dbc.query) === 'function')
                dbc.release();
            return null;
        },
	
        sql_error = function (err, sql) {
            if (!common.empty(sql))
                console.warn(sql);
            console.error(err);
        };
    
    function db(parent) {
        this.dbc = null;
        this.lastquery = '';
        this.elapsedms = '';
    }    ;
    
    db.prototype.begin = function (callback) {
        var me = this;
        async.waterfall(
            [
                function (cb) {
                    get_connection(function (err, dbcon) {
                        cb(err, dbcon);
                    });
                },
                function (dbcon, cb) {
                    dbcon.beginTransaction(function (err) { cb(err, dbcon); });
                },
            ], function (err, dbcon) {
                if (err) {
                    dbcon.release();
                    common.invokecallback(callback, err, false);
                } else {
                    me.dbc = dbcon;
                    common.invokecallback(callback, err, true);
                }
            });
    };
    
    db.prototype.query = function () {
        var me = this, 
            start = get_microtime(),
            params = get_parameter(arguments, ['sql']);
        async.waterfall([
            function (cb) {
                get_connection(function (err, dbc, isnew) { cb(err, dbc, isnew); });
            },
            function (dbc, isnew, cb) {
                dbc.query(params['sql'], function (err, result, fields) {
                    me.lastquery = params['sql'];
                    cb(err, dbc, isnew, result, fields);
                })
            },
        ], function (err, dbc, isnew, result, fields) {
            me.elapsedms = get_elapsedms(start);
            if (isnew === true)
                close_connection(dbc);
            if (err) {
                sql_error(err, params['sql']);
                common.invokecallback(params['callback'], err, null);
            } else
                common.invokecallback(params['callback'], err, result, fields);
        });
    };
    
    db.prototype.cell = function () {
        var params = get_parameter(arguments, ['table', 'column', 'condition', 'order', 'offset', 'group', 'having']);
        if (!common.empty(params['table'])) {
            if (typeof (params['column']) === 'object')
                params['column'] = common.first(params['column']).value;
            params['offset'] = common.tonumber(params['offset']);
            limit = common.empty(params['offset']) ? 1 : { limit: 1, offset: params['offset'] };
            this.query(get_selectsql(params['table'], params['column'], params['condition'], params['order'], limit, params['group'], params['having']), 
				function (err, result, fields) {
                common.invokecallback(params['callback'], err, err ? null : common.first(result[0]).value);
            });
        } else
            common.invokecallback(params['callback'], null, null);
    };
    
    db.prototype.sum = function () {
        var args = Array.prototype.slice.call(arguments);
        if (args.length > 1) {
            if (typeof (args[1]) === 'object')
                args[1] = common.first(args[1]).value;
            args[1] = "{DB_SUM}" + args[1];
            this.cell.apply(this, args);
        }
    };
    
    db.prototype.count = function () {
        var args = Array.prototype.slice.call(arguments);
        if (args.length > 1) {
            if (typeof (args[1]) === 'function') {
                args.push(args[1]);
                args[1] = "*";
            }
            if (typeof (args[1]) === 'object')
                args[1] = common.first(args[1]).value;
            args[1] = "{DB_COUNT}" + args[1];
            this.cell.apply(this, args);
        }
    };
    
    db.prototype.one = function () {
        var params = get_parameter(arguments, ['table', 'column', 'condition', 'order', 'offset', 'group', 'having']);
        if (!common.empty(params['table'])) {
            params['offset'] = common.tonumber(params['offset']);
            limit = common.empty(params['offset']) ? 1 : { limit: 1, offset: params['offset'] };
            this.query(get_selectsql(params['table'], params['column'], params['condition'], params['order'], limit, params['group'], params['having']), 
			function (err, result) {
                common.invokecallback(params['callback'], err, err ? null : result[0]);
            });
        } else
            common.invokecallback(params['callback'], null, null);
    };
    
    db.prototype.onerow = function () {
        var params = get_parameter(arguments, ['table', 'column', 'condition', 'order', 'offset', 'group', 'having']);
        if (!common.empty(params['table'])) {
            params['offset'] = common.tonumber(params['offset']);
            limit = common.empty(params['offset']) ? 1 : { limit: 1, offset: params['offset'] };
            this.query(get_selectsql(params['table'], params['column'], params['condition'], params['order'], limit, params['group'], params['having']), function (err, result) {
                common.invokecallback(params['callback'], err, err ? null : common.array_values(result[0]));
            });
        } else
            common.invokecallback(params['callback'], null, null);
    };
    
    db.prototype.select = function (table, column, condition, order, group, having, callback) {
        var params = get_parameter(arguments, ['table', 'column', 'condition', 'order', 'limit', 'group', 'having']);
        if (!common.empty(params['table'])) {
            this.query(get_selectsql(params['table'], params['column'], params['condition'], params['order'], params['limit'], params['group'], params['having']), function (err, result) {
                common.invokecallback(params['callback'], err, err ? null : result);
            });
        } else
            common.invokecallback(params['callback'], null, null);
    };
    
    db.prototype.insert = function (table, data) {
        var params = get_parameter(arguments, ['table', 'data']);
        if (!common.empty(params['table'])) {
            this.query(get_insertsql(params['table'], params['data']), function (err, result) {
                if (params.hasOwnProperty('callback') && typeof (params['callback']) === 'function')
                    common.invokecallback(params['callback'], err, err ? null : result);
            });
        } else if (params.hasOwnProperty('callback') && typeof (params['callback']) === 'function')
            common.invokecallback(params['callback'], null, err ? null : null);
    };
    
    db.prototype.update = function (table, column, condition, order, group, having, callback) {
        var params = get_parameter(arguments, ['table', 'data', 'condition', 'order', 'limit']);
        if (!common.empty(params['table'])) {
            this.query(get_updatesql(params['table'], params['data'], params['condition'], params['order'], params['limit']), function (err, result) {
                if (params.hasOwnProperty('callback') && typeof (params['callback']) === 'function')
                    common.invokecallback(params['callback'], err, err ? null : result);
            });
        } else if (params.hasOwnProperty('callback') && typeof (params['callback']) === 'function')
            common.invokecallback(params['callback'], null, null);
    };
    
    db.prototype.delete = function () {
        var params = get_parameter(arguments, ['table', 'condition', 'order', 'limit']);
        if (!common.empty(params['table'])) {
            this.query(get_updatesql(params['table'], params['condition'], params['order'], params['limit']), function (err, result) {
                if (params.hasOwnProperty('callback') && typeof (params['callback']) === 'function')
                    common.invokecallback(params['callback'], err, err ? null : result);
            });
        } else if (params.hasOwnProperty('callback') && typeof (params['callback']) === 'function')
            common.invokecallback(params['callback'], null, null);
    };
    
    db.prototype.commit = function (callback) {
        var me = this;
        if (me.dbc !== null) {
            me.dbc.commit(function (err) {
                sql_error(err, "commit");
                if (!err)
                    me.dbc = close_connection(me.dbc);
                common.invokecallback(callback, err, err ? false : true);
            });
        } else
            common.invokecallback(callback, null, false);
    };
    
    db.prototype.rollback = function (callback) {
        var me = this;
        if (me.dbc !== null) {
            me.dbc.rollback(function () {
                me.dbc = close_connection(me.dbc);
                common.invokecallback(callback, null, true);
            });
        } else
            common.invokecallback(callback, null, false);
    };
    
    db.prototype.destroy = function () {
        if (this.dbc !== null)
            this.dbc = close_connection(this.dbc);
    };
    
    module.exports = function (clusterobj) {
        cluster = clusterobj;
        return new db();
    };

})();
