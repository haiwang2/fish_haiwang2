(function() {
    var util = require('util'),
        events = new require('events'),
        _ = require('underscore');

    function timer() {
        events.EventEmitter.call(this);
        _.bindAll(this, 'start', 'stop', 'tick', 'delay');
        if (false === (this instanceof timer))
            return new timer();

        this.hour = 3600000;
        this.minute = 60000;
        this.second = 1000;
        this.time = this.hour;
        this.interval = 0;
        
    };

    util.inherits(timer, events.EventEmitter);

    timer.prototype.timestamp = function(milisecond) {
        return milisecond === true ? new Date().getTime() : Math.floor(new Date.now() / 1000);
    };

    timer.prototype.now = function() {
		var date = new Date(),
			year = date.getFullYear(),
			month = date.getMonth() + 1,
			day  = date.getDate(),
			hour = date.getHours(),
			min  = date.getMinutes(),
			sec  = date.getSeconds();

		hour = (hour < 10 ? "0" : "") + hour;
		min = (min < 10 ? "0" : "") + min;
		sec = (sec < 10 ? "0" : "") + sec;
		month = (month < 10 ? "0" : "") + month;
		day = (day < 10 ? "0" : "") + day;
		return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
    };

    timer.prototype.delay = function(delay, callback) {
        if (typeof(callback) === 'function')
            var timeout = setTimeout(function() {
                clearTimeout(timeout);
                callback(delay);
            }, delay);
    };

    timer.prototype.start = function(countdown, interval) {

        countdown = parseFloat(countdown);
        if (countdown > 0 && !isNaN(countdown))
            this.time = countdown;

        interval = parseFloat(interval);
        if (interval <= 0 || isNaN(interval))
            interval = this.second;

        this.interval = setInterval(this.tick, interval);
        this.emit('start');
    };

    timer.prototype.stop = function() {
        if (this.interval) {
            clearInterval(this.interval);
            this.emit('stop');
        }
    };

    timer.prototype.tick = function() {
        if (this.time === 0)
            this.stop();
        else {
            this.emit('tick', this.time);
            this.time--;
        }
    };

    module.exports = new timer;
})();