(function(){
	
	var common = require('./common'),
		eventemitter = require('events').EventEmitter,
		util = require('util'),
		get_parameter = function(args, param){
			var result = {}, ended = false;
			if(!common.empty(args)){		
				for(var i in args){
					if(typeof(args[i]) === 'function'){
						result['callback'] = args[i];
						ended = true;
					}else{
						if(ended === false)
							result[param[i]] = args[i];
						else
							result[param[i]] = undefined;
					}
				}
			}else{
				for(var i in param)
					result[param[i]] = undefined;
			}
			return result;
		};

	function client(){
		eventemitter.call(this);

		var me = this;
		me.connected = false;
		me.received = '';
		me.sent = '';
		me.socket = null;
		me.net = require('net');
	};
	util.inherits(client, eventemitter);
	
	client.prototype.error = function(error, client){
		this.emit('socketerror', error.code, client);
	};

	client.prototype.connect = function(){
		var me = this, params = get_parameter(arguments, ['port', 'ip']);
		params['port'] = common.tonumber(params['port']);
		params['ip'] = common.tostring(params['ip']);
		params['ip'] = params['ip'] === '' ? '127.0.0.1' : params['ip'];
		me.socket = new this.net.createConnection(params['port'], params['ip']);
		me.socket.on('connect', function(){
			me.connected = true;
			me.socket.on('error', function(error){me.error(error, me);});
			me.socket.on('data', function(data){me.receive(data);});
			me.socket.on('close', function(){me.disconnect(me);});
			common.invokecallback(me, params['callback']);
		});
	};

	client.prototype.receive = function(transmit){
		var data = {};
		this.received = transmit.toString();
		try{
			data = JSON.parse(this.received);
		}catch(e){}
		if(data.hasOwnProperty('event') && data.hasOwnProperty('content'))
			this.emit(data.event, data.content);
	};

	client.prototype.send = function(event, content){
		if(this.connected === true){
			var transmit = '';
			try{
				transmit = JSON.stringify({event:event, content:content});
			}catch(e){}
			if(transmit !== ''){
				this.sent = transmit;
				return this.socket.write(transmit);
			}else
				return false;
		}else{
			console.log('not yet connected');
			return false;
		}
	};

	client.prototype.disconnect = function(transmit){
		this.connected = false;
		this.emit('disconnect');
	};

    module.exports = new client;

})();