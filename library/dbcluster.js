(function(){
	
	var common = require('./common');

	function cluster(config) {
		config = config.db;
		this.dbcluster = require('mysql').createPoolCluster(config.cluster);
		if(config.hasOwnProperty('nodes') && !common.empty(config.nodes) && typeof(config.nodes) === 'object')
			for (var name in config.nodes)
				this.dbcluster.add(name, config.nodes[name]);
	}
	
	cluster.prototype.getconnection = function(callback){
		this.dbcluster.getConnection(callback);
	};

	cluster.prototype.end = function(callback){
		this.dbcluster.end(callback);
	};

	module.exports = function(config){
		return new cluster(config);
	};
})();

