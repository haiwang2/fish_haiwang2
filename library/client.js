(function () {
    
    var common = require('./common');
    
    function client(socket, cluster) {
        var me = this;        
        me.socket = socket;
        me.params = {};
        me.db = require('./db')(cluster);
        me.kiosk = require('../module/kiosk')(this);        
        me.buffer = {
            respone : {
                action : '',
                status : 'ok',
                error : null,
                data : {}
            },
            broadcast : {
                action : '',
                status : 'ok',
                error : null,
                data : {}
            }
        },
		me.debugs = [];
       
    };
    
    client.prototype.clear = function () {
        this.buffer = {
            respone : {
                action : '',
                status : 'ok',
                error : null,
                buffer : {},
                data: {}
            },
            broadcast : {
                action : '',
                status : 'ok',
                error : null,
                buffer : {},
                data: {}
            }
        };
        this.debugs = [];
    };
    
    client.prototype.debug = function (debug) {
        var me = this;
        if (debug) {
            try {
                var syslog = require('node-syslog');
                if (typeof (debug) === 'object' || typeof (debug) === 'array')
                    debug = JSON.stringify(debug);
                syslog.init("node-syslog", Syslog.LOG_PID | Syslog.LOG_ODELAY, Syslog.LOG_LOCAL0);
                syslog.log(Syslog.LOG_NOTICE, debug);
                syslog.close();
            } catch (e) { }
            me.debugs.push(debug);
            console.log('client ' + me.socket.id + ': ' + debug);
        }
    };
    
    client.prototype.error = function (errtype, errcode, debug) {
        if (errtype && errcode) {
            switch (typeof (debug)) {
                case 'undefined':
                case 'null':
                    debug = null;
                    break;

                case 'object':
                    if (debug.hasOwnProperty('message'))
                        debug = debug.message;
                    break;
            }
            
            if (debug)
                this.debug(common.tostring(debug));
            
            this.buffer.respone.status = errtype;
            this.buffer.respone.error = errcode;
        }
    };
    
    client.prototype.respone = function (name, data) {
        if (common.empty(data)) {
            if (this.buffer.respone.data.hasOwnProperty(name))
                return this.buffer.respone.data[name];
            else
                return undefined;
        } else            
            this.buffer.respone.data[name] = data;
    };
    
    client.prototype.broadcast = function (name, data) {
        if (common.empty(data)) {
            if (this.broadcast.buffer.data.hasOwnProperty(name))
                return this.buffer.broadcast.data[name];
            else
                return undefined;
        } else
            this.buffer.broadcast.data[name] = data;
    };
    
    client.prototype.output = function (action_respone, action_broadcast) {
        if (!common.empty(this.buffer.respone.data) && typeof (this.buffer.respone.data) === 'object' && this.buffer.respone.data !== null) {
            var data = this.buffer.respone.data;
            this.buffer.respone.debug = this.debugs;
            kiosk = this.kiosk;
            if (action_respone.status === 'ok' && !common.empty(kiosk))
                this.buffer.respone.user = {
                    id: kiosk.kiosk_id,
                    name: kiosk.kiosk_username,
                    pointa: kiosk.kiosk_balance_a * me.client.ptmultiplier,
                    pointb: kiosk.kiosk_balance_b * me.client.ptmultiplier,
                    pointc: kiosk.kiosk_balance_c * me.client.ptmultiplier,
                };
            delete this.buffer.respone.data;
            this.buffer.respone = common.mergeobj(this.buffer.respone, data);
            this.socket.send(action_respone, this.buffer.respone);
        }
        
        if (!common.empty(this.buffer.broadcast.data) && typeof (this.buffer.broadcast.data) === 'object' && this.buffer.broadcast.data !== null) {
            var data = this.buffer.broadcast.data;
            this.buffer.broadcast.debug = this.debugs;
            delete this.buffer.broadcast.data;
            this.buffer.broadcast = common.mergeobj(this.buffer.broadcast, data);
            this.socket.broadcast(action_broadcast, this.buffer.broadcast);
        }
        
        this.clear();
    };
    
    client.prototype.setverify = function (flag) {
        if(this.socket!=undefined)
            return this.socket.verify = flag;
        return;
    }
    
    client.prototype.getverify = function (flag) {
        if (this.socket != undefined)
            return this.socket.verify;
        return false;
    }
    
    client.prototype.closed = function (correct) {
        if (this.socket != undefined)
            return this.socket.closed(correct);
    }

    module.exports = function (socket, cluster) {
        return new client(socket, cluster);
    };

})();