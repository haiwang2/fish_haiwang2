(function () {
    
    var common = require('./common');
    
    function server(config, port) {
        var me = this;
        
        me.net = require('./netserver'); //导入network对象。
        me.cluster = require('./dbcluster')(config); //载入多节点Mysql-Cluster的控制对象
        me.db = require('./db')(me.cluster); //载入资料库active record对象
        
        me.buffer = {
            action : '',
            status : 'ok',
            error : null,
            data : {}
        };
        
        me.clients = {};
        
        me.debugs = [];
        
        //port = common.tonumber(port);
       // if (port > 1024)
          me.net.listen(8080);
    }
    
    server.prototype.clear = function () {
        this.buffer = {
            action : '',
            status : 'ok',
            error : null,
            data : {}
        };
        this.debugs = [];
    };
    
    server.prototype.debug = function (debug) {
        if (debug) {
            try {
                var syslog = require('node-syslog');
                if (typeof (debug) === 'object' || typeof (debug) === 'array')
                    debug = JSON.stringify(debug);
                syslog.init("node-syslog", Syslog.LOG_PID | Syslog.LOG_ODELAY, Syslog.LOG_LOCAL0);
                syslog.log(Syslog.LOG_NOTICE, debug);
                syslog.close();
            } catch (e) { }
            this.debugs.push(debug);
            console.log('server: ' + debug);
        }
    };
    
    server.prototype.broadcast = function (name, data) {
        if (common.empty(data)) {
            if (this.buffer.data.hasOwnProperty(name))
                return this.data[name];
            else
                return undefined;
        } else
            this.buffer.data[name] = data;
    };
    
    server.prototype.output = function (action_broadcast) {
        if (!common.empty(this.buffer.data) && typeof (this.buffer.data) === 'object' && this.buffer.data !== null) {
            var output = this.buffer.data;
            this.buffer.debug = this.debugs;
            delete this.buffer.data;
            this.buffer = common.mergeobj(this.buffer, output);
            this.net.broadcast(action_broadcast, this.buffer);
        }
        this.clear();
    };
    
    server.prototype.getusers = function (){
        var users = {};
        for (var id in this.clients) {
            if (this.clients[id].socket.verify) {
                users[id] = this.clients[id];
            }
        }
        return users;
    }
    
    module.exports = function (config) {
        return new server(config);
    };

})();